<?php

/*
 * L commponent
 * 
 */

namespace app\components;

class L
{
    private static $messages;
    private static $ready = false;
    private static $metaTags = array();

    public static function init()
    {
        self::$messages = $GLOBALS['_Messages'];
        self::$ready = true;
    }

    public static function t($const)
    {
        if (self::$ready == false)
            self::init();
        return self::$messages[$const];
    }

    public static function setMeta($tag, $value)
    {
        self::$metaTags[$tag] = CHtml::encode($value);
    }

    public static function getMeta($tag)
    {
        if (isset(self::$metaTags[$tag]))
        {
            return self::$metaTags[$tag];
        }
        else
        {
            return false;
        }
    }
    
    public static function timeAgo($datetime, $full = false) 
    {
        $now = new \DateTime;
        $ago = new \DateTime($datetime);
        $diff = $now->diff($ago);

        $diff->w = floor($diff->d / 7);
        $diff->d -= $diff->w * 7;

        $string = array(
            'y' => 'year',
            'm' => 'month',
            'w' => 'week',
            'd' => 'day',
            'h' => 'hour',
            'i' => 'minute',
            's' => 'second',
        );
        foreach ($string as $k => &$v) {
            if ($diff->$k) {
                $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
            } else {
                unset($string[$k]);
            }
        }

        if (!$full) $string = array_slice($string, 0, 1);
        return $string ? implode(', ', $string) . ' ago' : 'just now';
    }
    /**
     * replace specialchars in string to get good string to use in url 
     * @param string $string old string
     *
     * @return string new string
     */
    public static function urlChars($string)
    {

        $array = array(
            //WIN
            "\xb9"     => "a", "\xa5"     => "A", "\xe6"     => "c", "\xc6"     => "C",
            "\xea"     => "e", "\xca"     => "E", "\xb3"     => "l", "\xa3"     => "L",
            "\xf3"     => "o", "\xd3"     => "O", "\x9c"     => "s", "\x8c"     => "S",
            "\x9f"     => "z", "\xaf"     => "Z", "\xbf"     => "z", "\xac"     => "Z",
            "\xf1"     => "n", "\xd1"     => "N",
            //UTF
            "\xc4\x85" => "a", "\xc4\x84" => "A", "\xc4\x87" => "c", "\xc4\x86" => "C",
            "\xc4\x99" => "e", "\xc4\x98" => "E", "\xc5\x82" => "l", "\xc5\x81" => "L",
            "\xc3\xb3" => "o", "\xc3\x93" => "O", "\xc5\x9b" => "s", "\xc5\x9a" => "S",
            "\xc5\xbc" => "z", "\xc5\xbb" => "Z", "\xc5\xba" => "z", "\xc5\xb9" => "Z",
            "\xc5\x84" => "n", "\xc5\x83" => "N",
            //ISO
            "\xb1"     => "a", "\xa1"     => "A", "\xe6"     => "c", "\xc6"     => "C",
            "\xea"     => "e", "\xca"     => "E", "\xb3"     => "l", "\xa3"     => "L",
            "\xf3"     => "o", "\xd3"     => "O", "\xb6"     => "s", "\xa6"     => "S",
            "\xbc"     => "z", "\xac"     => "Z", "\xbf"     => "z", "\xaf"     => "Z",
            "\xf1"     => "n", "\xd1"     => "N",
            //HTML
            "&#260;"   => "A", "&#261;"   => "a", "&#280;"   => "E",
            "&#281;"   => "e", "&#211;"   => "O", "&#243;"   => "o",
            "&Oacute;" => "O", "&oacute;" => "o", "&#262;"   => "C",
            "&#263;"   => "c", "&#321;"   => "L", "&#322;"   => "l",
            "&#323;"   => "N", "&#324;"   => "n", "&#346;"   => "S",
            "&#347;"   => "s", "&#377;"   => "Z", "&#378;"   => "z",
            "&#379;"   => "Z", "&#380;"   => "z",
            //SPACEBAR
            " "        => "-"
        );

        $string = strtr($string, $array);
        $string = preg_replace("/[^\w-|\+]/", '', $string);

        return $string;
    }

    /**
     * Return formatted date from unixtime
     * @param int $unix_time
     * @param bool $show_time
     * @return string
     */
    public static function getFormattedDate($unix_time, $show_time = false)
    {
        if ($unix_time == 0)
        {
            return '';
        }
        return ($show_time ? date('Y.m.d H:i', $unix_time) : date('Y.m.d', $unix_time));
    }

    public static function calculateAge($date)
    {

        $today = explode(" ", date('d m Y'));
        $birthday = explode("-", L::getBirthDate($date));

        $age = $today[2] - $birthday[2];

        if ($today[1] > $birthday[1] || $today[1] == $birthday[1] && $today[0] > $birthday[0])
            $age--;

        return $age;
    }

    /**
     * Converts  RRRMMDD foramt date to  DD-MM-RRRR format
     * @param int $date - date in RRRMMDD format
     * @return string date
     */
    public static function getBirthDate($date)
    {
        $day = $date % 100;
        $date = (int) ($date / 100);
        $month = $date % 100;
        $year = (int) ($date / 100);
        return $day . '-' . $month . '-' . $year;
    }

    /**
     * Converts DD-MM-RRRR foramt date to RRRMMDD format
     * @param string $date - date in DD-MM-RRRR format
     * @return int date
     */
    public static function getBirthDateRev($date)
    {
        $pieces = explode("-", $date);
        return (int) $pieces[2] * 10000 + (int) $pieces[1] * 100 + (int) $pieces[0];
    }

    /**
     * Converts facebbok foramt date to RRRMMDD format
     * @param string $date - date in facebook format
     * @return int date
     */
    public static function FbBirthday($date)
    {
        $pieces = explode("/", $date);
        return (int) $pieces[2] * 10000 + (int) $pieces[0] * 100 + (int) $pieces[1];
    }
    
    public static function unique_id($l = 8)
    {
        $better_token = md5(uniqid(rand(), true));
        $rem = strlen($better_token)-$l;
        $unique_code = substr($better_token, 0, -$rem);
        $uniqueid = $unique_code;
        return $uniqueid;
  }
    /**
     * ucfirst UTF-8 aware function
     *
     * @param string $string
     * @return string
     */
    public static function ucfirst($string, $e = 'utf-8')
    {
        if (function_exists('mb_strtoupper') && function_exists('mb_substr') && !empty($string))
        {
            $string = mb_strtolower($string, $e);
            $upper = mb_strtoupper($string, $e);
            preg_match('#(.)#us', $upper, $matches);
            $string = $matches[1] . mb_substr($string, 1, mb_strlen($string, $e), $e);
        }
        else
        {
            $string = ucfirst($string);
        }
        return $string;
    }

    public static function checkLead($lead)
    {
        if (strtolower(substr($lead, 0, 3)) != '<p>')
        {
            $lead = '<p>' . $lead . '</p>';
        }

        return $lead;
    }

    /**
     * generate new random password
     * @param string $oldhash seed
     * @return string - new password 
     */
    public static function newPassword($oldhash)
    {
        return substr(md5($oldhash . "newPassword" . uniqid()), 0, 10);
    }

    public static function textCut($text, $limit)
    {
        if (strlen($text) > $limit)
            $text = substr($text, 0, $limit) . '...';

        return $text;
    }

    public static function sublead($lead, $length)
    {

        $lead_a = explode(" ", $lead);

        $return = '';

        foreach ($lead_a as $v)
        {
            if (strlen($return . ' ' . $v) > $length)
                break;
            $return .= ' ' . $v;
        }

        $return = substr($return, 1);
		
		if(strstr($return, '<p>') && !strstr($return, '</p>')) $return .= '</p>';
		
        return $return . ($lead != $return ? '...' : '');
    }
	
    public static function randomString($length=15)
    {
            $return = '';

            for($i=0;$i<$length;$i++) {
                    $return .= rand(0,1) ? strtolower(chr(65+rand(0,25))) : chr(65+rand(0,25));
            }

            return $return;
    }

    public static function stripForumLead($lead) {
            $lead = str_replace('&nbsp;', ' ', $lead);
            $lead = str_replace('&oacute;', 'ó', $lead);
            $lead = str_replace('&Oacute;', 'Ó', $lead);		

            return $lead;
    }
}

?>
