<?php

namespace app\components;

use yii\base\Widget;
use yii\helpers\Html;
use yii\helpers\Url;
use app\models\Paste;
use app\components\L;


class RecentlyAdded extends Widget
{
    public $limit;
    public $html;
    
    public function init()
    {
        parent::init();
        
        $this->limit = 5;
        $pastes = Paste::findRecentlyAdded($this->limit);
        
        $this->html = '<table id="example1" class="table table-bordered table-hover">
        <tbody>';
        foreach ($pastes as $paste) {
            $pasteTitle = Html::encode($paste->title);
            $pasteUrl = Url::toRoute('/paste/' . $paste->getId() );
            $pasteType = $paste->highlighting->name;
            $pasteAdded = L::timeAgo($paste->insert_date);
            
            $this->html .= '<tr>
                  <td class="col-md-5"><a href="'.$pasteUrl .'" title="'. $pasteTitle .'"><span class="pull-left badge bg-blue">'.strtoupper($paste->highlighting->name).'</span>&nbsp;'.$pasteTitle .'</a><br/>'.$pasteType.' | '.$pasteAdded.'</td>
              </tr>';
        }            
        $this->html .= '</tbody>
      </table>';
        
    }

    public function run()
    {
        return $this->html;
    }
}