<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'name' => 'PasteBIN',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'WOW_9iiGYJMCC1yCEYwLW_w-bXGuupOu',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'sc' => [
            'class' => 'app\components\SrcCollect',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => false,
            'rules' => [
                'login'         => 'site/login',
                'registration'  => 'site/registration',
                'forgotten'     => 'site/forgotten',
                'fblogin'       => 'site/fblogin',
                'gglogin'       => 'site/gglogin',
                'twlogin'       => 'site/twlogin',
                'terms'         => 'site/terms',
                'trends'        => 'site/trends',
                'about'         => 'site/about',
                'privacy'       => 'site/privacy',
                'sitemap.xml'   => 'site/sitemap',
                'paste/<id:.+>' => 'site/index',
                'user/<id:.+>'  => 'user/index',
                //'user/<id:.+>/<tab:\d+>' => 'user/index',
                'search/<id:.+>'  => 'search/index',
                //'admin'         => 'admin/index',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
		'<controller>/<action>/<id:.+>' => '<controller>/<action>'
            ],
        ],
        'view' => [
            'theme' => [
                'pathMap' => [
                   '@app/views' => '@app/views'
                ],
            ],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'bundles' => [
                'dmstr\web\AdminLteAsset' => [
                    'skin' => 'skin-blue',
                ],
            ],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
