<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use app\models\Paste;
use app\models\Comment;
use app\models\AdminPasteForm;
use app\models\User;
use app\models\AdminUserForm;
use app\models\UserGroup;
use app\models\UserGroupPermission;
use app\models\AdminUserGroupForm;
use app\models\PasteStatus;
use app\models\Highlighting;
use app\models\Setting;
use app\models\AdminSettingsForm;
use app\models\AdminAuthenticationFacebookForm;
use app\models\AdminAuthenticationGoogleForm;
use app\models\AdminAuthenticationTwitterForm;

class AdminController extends Controller
{
    public $layout = 'admin';
    
    private function getAllPermissions()
    {
        $permissions = [
            'pastes' => 0, 
            'paste-create' => 0,
            'paste-edit' => 0,
            'paste-remove' => 0,
            'users' => 0, 
            'user-create' => 0,
            'user-edit' => 0,
            'user-remove' => 0,
            'user-groups' => 0,
            'user-group-create' => 0,
            'user-group-edit' => 0,
            'user-group-remove' => 0,
        ];

        return $permissions;
    }
    
    private function accessControl($name = null)
    {
        if (Yii::$app->user->isGuest)
        {
            return $this->redirect(Url::toRoute('/zaloguj'));
        }
        elseif (!Yii::$app->user->identity->isAdmin())
        {
            return $this->redirect(Url::home());
        }
        elseif ($name)
        {
            if (!Yii::$app->user->identity->hasPrivilege($name))
            {
                return $this->redirect(Url::home());
            }
        }

        return null;
    }

    public function actionIndex()
    {
        if ($result = $this->accessControl()) return $result;
        
        $UsersList = User::find()->orderBy('user_create_date desc')->limit('8')->all();
        $countUsers = User::countAllUsers();
        $countPastes = Paste::countAllPastes();
        $countComments = Comment::countAllComments();
        return $this->render('index', [
            'newUsersList' => $UsersList,
            'countUsers' => $countUsers,
            'countPastes' => $countPastes,
            'countComments' => $countComments,
        ]);
    }
    
    /*
     * Pastes actions
     */
    
    public function actionPastes()
    {
        if ($result = $this->accessControl('pastes')) return $result;
        
        $PastesList = Paste::find()->all();
        
        return $this->render('pastes', [
            'pastesList' => $PastesList,
        ]);
    }
    
    public function actionPasteCreate()
    {
        if ($result = $this->accessControl('paste-create')) return $result;
        
        $formModel = new AdminPasteForm();
            
        if (Yii::$app->request->isPost)
        {
            if (isset($_POST['save-button']))
            {
                if ($formModel->load(Yii::$app->request->post()) && $formModel->save())
                {
                    Yii::$app->session->setFlash('pasteFormSubmitted');
                    return $this->redirect(Url::toRoute('/admin/pastes'));
                }
            }
            if (isset($_POST['save-and-stay-button']))
                {
                    if ($formModel->load(Yii::$app->request->post()) && $formModel->save())
                    {
                        Yii::$app->session->setFlash('pasteFormSubmitted');
                        $paste = $formModel->getLastModel();  
                        $id = $paste->getId();
                        return $this->redirect(Url::toRoute('/admin/paste-edit/'.$id));
                    }
                }
            elseif (isset($_POST['cancel-button']))
            {
                return $this->redirect(Url::toRoute('/admin/pastes'));
            }
        }
        
        $highlighting = ArrayHelper::map(Highlighting::find()->all(), 'highlighting_id', 'name');
        $status = ArrayHelper::map(PasteStatus::find()->all(), 'pastestatus_id', 'name');
        $tags = ArrayHelper::map(PasteStatus::find()->all(), 'pastestatus_id', 'name');
        $users = ArrayHelper::map(User::find()->all(), 'user_id', 'user_name');

        return $this->render('paste-create', [
            'model' => $formModel,
            'highlighting' => $highlighting,
            'status' => $status,
            'tags' => $tags,
            'users' => $users,
        ]);
        
    }
    
    public function actionPasteEdit($id)
    {
        if ($result = $this->accessControl('paste-edit')) return $result;
 
        if ( $paste = Paste::findById($id) )
        {
            $formModel = new AdminPasteForm();
			
            $formModel->content = $paste->content;
            $formModel->title   = $paste->title;
            $formModel->highlighting_id = $paste->highlighting_id;
            $formModel->tags = $paste->tags;
            $formModel->status_id = $paste->status_id;
            $formModel->views = $paste->views;
            $formModel->active_from = $paste->active_from;
            $formModel->active_to = $paste->active_to;
            $formModel->insert_date = $paste->insert_date;
            $formModel->user_id = $paste->user_id;
            $formModel->allowed_users = $paste->allowed_users;
            
            if (Yii::$app->request->isPost)
            {
                if (isset($_POST['save-button']))
                {
                    if ($formModel->load(Yii::$app->request->post()) && $formModel->save($id))
                    {
                        $post = Yii::$app->request->post();                        
                        return $this->redirect(Url::toRoute('/admin/pastes'));
                    }
                }
                if (isset($_POST['save-and-stay-button']))
                {
                    if ($formModel->load(Yii::$app->request->post()) && $formModel->save($id))
                    {
                        $post = Yii::$app->request->post();
                        return $this->redirect(Url::toRoute('/admin/paste-edit/'.$id));
                    }
                }
                elseif (isset($_POST['cancel-button']))
                {
                    return $this->redirect(Url::toRoute('/admin/pastes'));
                }
            }
            
            $highlighting = ArrayHelper::map(Highlighting::find()->all(), 'highlighting_id', 'name');
            $status = ArrayHelper::map(PasteStatus::find()->all(), 'pastestatus_id', 'name');
            $tags = ArrayHelper::map(PasteStatus::find()->all(), 'pastestatus_id', 'name');
            $users = ArrayHelper::map(User::find()->all(), 'user_id', 'user_name');
        
            return $this->render('paste-edit', [
                'paste' => $paste,
                'model' => $formModel,
                'highlighting' => $highlighting,
                'status' => $status,
                'tags' => $tags,
                'users' => $users,
            ]);
        }
        else
        {
            return $this->redirect(Url::toRoute('/admin/pastes'));
        }        
    }
    
    public function actionPasteRemove($id)
    {
        if ($result = $this->accessControl('paste-remove')) return $result;
		
        if ( $paste = Paste::findById($id) )
        {
            $paste->delete();
        }

        return $this->redirect(Url::toRoute('admin/pastes'));
    }
    
    /*
     * Users actions
     */
    
    public function actionUsers()
    {
        if ($result = $this->accessControl('users')) return $result;
        
        $UsersList = User::find()->all();
        
        return $this->render('users', [
            'usersList' => $UsersList,
        ]);
    }
    
    public function actionUserCreate()
    {
        if ($result = $this->accessControl('user-create')) return $result;
        
        $formModel = new AdminUserForm();
            
        if (Yii::$app->request->isPost)
        {
            if (isset($_POST['save-button']))
            {
                if ($formModel->load(Yii::$app->request->post()) && $formModel->save())
                {                    
                    Yii::$app->session->setFlash('userFormSubmitted');
                    return $this->redirect(Url::toRoute('/admin/users'));
                }
            }                
            if (isset($_POST['save-and-stay-button']))
            {
                if ($formModel->load(Yii::$app->request->post()) && $formModel->save())
                {   
                    Yii::$app->session->setFlash('userFormSubmitted');
                    $user = $formModel->getLastModel();  
                    $id = $user->getId();
                    return $this->redirect(Url::toRoute('/admin/user-edit/'.$id));
                }
            }
            elseif (isset($_POST['cancel-button']))
            {
                return $this->redirect(Url::toRoute('/admin/users'));
            }
        }
            
        $normal_user_group = new UserGroup();
        $normal_user_group->user_group_id = 0;
        $normal_user_group->user_group_name = 'User';
        $normal_user_group = [$normal_user_group];

        $database_user_group = UserGroup::find()->all();
        $all_user_group = ArrayHelper::merge($normal_user_group, $database_user_group);

        $user_group = ArrayHelper::map($all_user_group, 'user_group_id', 'user_group_name');

        return $this->render('user-create', [
            'model' => $formModel,
            'user_group' => $user_group,
        ]);
    }
    
    public function actionUserEdit($id)
    {
        if ($result = $this->accessControl('user-group-edit')) return $result;
 
        if ( $user = User::findIdentity($id) )
        {
            $formModel = new AdminUserForm();
            $formModel->status      = $user->user_status;
            $formModel->username    = $user->user_name;
            $formModel->firstname   = $user->user_first_name;
            $formModel->lastname    = $user->user_last_name;
            $formModel->email       = $user->user_email;
            $formModel->sex         = $user->user_sex;            
            $formModel->about       = $user->user_description;
            $formModel->userAdminType   = $user->user_administrator_type_id;
            
            if (Yii::$app->request->isPost)
            {
                if (isset($_POST['save-button']))
                {
                    if ($formModel->load(Yii::$app->request->post()) && $formModel->save($id))
                    {
                        return $this->redirect(Url::toRoute('/admin/users'));
                    }
                }                
                if (isset($_POST['save-and-stay-button']))
                {
                    if ($formModel->load(Yii::$app->request->post()) && $formModel->save($id))
                    {   
                        return $this->redirect(Url::toRoute('/admin/user-edit/'.$id));
                    }
                }
                elseif (isset($_POST['cancel-button']))
                {
                    return $this->redirect(Url::toRoute('/admin/users'));
                }
            }
            
            $normal_user_group = new UserGroup();
            $normal_user_group->user_group_id = 0;
            $normal_user_group->user_group_name = 'User';
            $normal_user_group = [$normal_user_group];
            
            $database_user_group = UserGroup::find()->all();
            $all_user_group = ArrayHelper::merge($normal_user_group, $database_user_group);
            
            $user_group = ArrayHelper::map($all_user_group, 'user_group_id', 'user_group_name');
            
            return $this->render('user-edit', [
                'user' => $user,
                'model' => $formModel,
                'user_group' => $user_group,
            ]);
        }
        else
        {
            return $this->redirect(Url::toRoute('/admin/users'));
        }
        
    }
    
    /*
     * User groups actions
     */
    
    public function actionUserGroups()
    {
        if ($result = $this->accessControl('user-groups')) return $result;
        
        $UserGroupsList = UserGroup::find()->all();
        
        return $this->render('user-groups', [
            'groupList' => $UserGroupsList,
        ]);
    }
    
    public function actionUserGroupCreate()
    {
        if ($result = $this->accessControl('user-group-create')) return $result;
        
        $formModel = new AdminUserGroupForm();
            
        if (Yii::$app->request->isPost)
        {
            if (isset($_POST['save-button']))
            {
                if ($formModel->load(Yii::$app->request->post()) && $formModel->save())
                {
                    return $this->redirect(Url::toRoute('/admin/user-groups'));
                }
            }
            elseif (isset($_POST['cancel-button']))
            {
                return $this->redirect(Url::toRoute('/admin/user-groups'));
            }
        }

        return $this->render('user-group-create', [
            'formModel' => $formModel,
        ]);
        
    }
        
    public function actionUserGroupEdit($id)
    {
        if ($result = $this->accessControl('user-group-edit')) return $result;
 
        if ( $group = UserGroup::findIdentity($id) )
        {
            $formModel = new AdminUserGroupForm();
			
            $formModel->name = $group['user_group_name'];
            
            if (Yii::$app->request->isPost)
            {
                if (isset($_POST['save-button']))
                {
                    if ($formModel->load(Yii::$app->request->post()) && $formModel->save($id))
                    {
                        $post = Yii::$app->request->post();
                        $group->deleteAllPermissions();
                        if (isset($post['permissions'])) {                            
                            foreach ($post['permissions'] as $permission_name => $permission_value)
                            {
                                $group->addPermissions($permission_name);
                            }
                        }
                        return $this->redirect(Url::toRoute('/admin/user-groups'));
                    }
                }
                elseif (isset($_POST['cancel-button']))
                {
                    return $this->redirect(Url::toRoute('/admin/user-groups'));
                }
            }
            $allPermissions = $this->getAllPermissions();            
            $allAssignedPermission = UserGroupPermission::find()->where('user_group_permission_type_id = '.$group->getId() )->orderBy('user_group_permission_code ASC')->all();
            
            foreach ($allAssignedPermission as $permission)
            {
                if (isset($allPermissions[$permission->user_group_permission_code])) {
                    $allPermissions[$permission->user_group_permission_code] = 1;
                }
            }
            return $this->render('user-group-edit', [
                'group' => $group,
                'permissions' => $allPermissions,
                'formModel' => $formModel,
            ]);
        }
        else
        {
            return $this->redirect(Url::toRoute('/admin/user-groups'));
        }
        
    }
    
    public function actionUserGroupRemove($id)
    {
        if ($result = $this->accessControl('user-group-remove')) return $result;
		
        if ( $group = UserGroup::findIdentity($id) )
        {
            $group->deleteAllPermissions();
            $group->delete();
        }

        return $this->redirect(Url::toRoute('admin/user-groups'));
    }
    
    /*
     * Settings actions
     */
    public function actionSettings()
    {
        $formModel = new AdminSettingsForm();
        return $this->render('settings', [
            'model' => $formModel,
        ]);
    }
    
    /*
     * Settings actions
     */
    public function actionAuthentication()
    {
        $formModel = new AdminSettingsForm();
        
        $facebookModel = new AdminAuthenticationFacebookForm();
        $facebookModel->active      = Setting::getValue('AUTH_FACEBOOK_API_ACTIVE');
        $facebookModel->app_id      = Setting::getValue('AUTH_FACEBOOK_API_APP_ID');
        $facebookModel->app_secret  = Setting::getValue('AUTH_FACEBOOK_API_APP_SECRET');
        
        $googleModel = new AdminAuthenticationGoogleForm();
        $googleModel->active        = Setting::getValue('AUTH_GOOGLE_API_ACTIVE');
        $googleModel->client_id     = Setting::getValue('AUTH_GOOGLE_API_CLIENT_ID');
        $googleModel->client_secret = Setting::getValue('AUTH_GOOGLE_API_CLIENT_SECRET');
        
        $twitterModel = new AdminAuthenticationTwitterForm();
        $twitterModel->active       = Setting::getValue('AUTH_TWITTER_API_ACTIVE');
        $twitterModel->api_key      = Setting::getValue('AUTH_TWITTER_API_CONSUMER_KEY');
        $twitterModel->api_secret   = Setting::getValue('AUTH_TWITTER_API_CONSUMER_SECRET');
        
        if (Yii::$app->request->isPost)
        {
            if (isset($_POST['facebook-save-button']))
            {
                if ($facebookModel->load(Yii::$app->request->post()) && $facebookModel->save())
                {
                    //add massage
                    Yii::$app->session->setFlash('admin_authentication', 'Changes have been saved.');
                    return $this->redirect(Url::toRoute('admin/authentication'));
                }
            }
            elseif (isset($_POST['google-save-button']))
            {
                if ($googleModel->load(Yii::$app->request->post()) && $googleModel->save())
                {
                    //add massage
                    Yii::$app->session->setFlash('admin_authentication', 'Changes have been saved.');
                    return $this->redirect(Url::toRoute('admin/authentication'));
                }
            }
            elseif (isset($_POST['twitter-save-button']))
            {
                if ($twitterModel->load(Yii::$app->request->post()) && $twitterModel->save())
                {
                    //add massage
                    Yii::$app->session->setFlash('admin_authentication', 'Changes have been saved.');
                    return $this->redirect(Url::toRoute('admin/authentication'));
                }
            }
        }
            
        return $this->render('authentication', [
            'model' => $formModel,
            'facebook' => $facebookModel,
            'google' => $googleModel,
            'twitter' => $twitterModel,
        ]);
    }
    
    
}