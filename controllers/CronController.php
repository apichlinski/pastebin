<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Paste;

class CronController extends Controller
{
    public function actionIndex()
    {
        /*
         * MAIN LOOP - Removing old paste from database
         */
        $time = new \DateTime('now');
        $dateTo = $time->format('Y-m-d H:i:s');
        $pasteToRemove = Paste::find()->where("active_to  <= NOW()" )->all();
        foreach ($pasteToRemove as $paste){
            $paste->delete();
        }
        
    }
}