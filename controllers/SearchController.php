<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Paste;
use app\models\User;

class SearchController extends Controller
{
    public function actionIndex($id=null)
    {
        if (Yii::$app->request->isAjax)
        {
            
        }
        if (Yii::$app->request->isPost || $id!='')
        {
            if (isset($_POST['Search']['query']) || $id!='') {
                $query = isset($_POST['Search']['query'])?$_POST['Search']['query']:$id;
                $users = null;
                $pastes = null;
                if (strlen($query)>3) {
                    $users = User::find()->where('user_name like :query', ['query'=>'%'.$query.'%'])->all();
                    $pastes = Paste::find()->where('status_id = :status and content like :query or title like :query', ['status'=>Paste::STATUS_PUBLISHED, 'query'=>'%'.$query.'%'])->all();
                }
                
            }
            return $this->render('index', [
                'usersList' => $users,
                'pastesList' => $pastes,
                'query' => $query,
            ]);
        }
        else 
        {
            
        }
    }
}