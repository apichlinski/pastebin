<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\RegisterForm;
use app\models\ForgottenForm;
use app\models\ContactForm;
use yii\helpers\Url;
use app\models\User;
use app\models\Paste;
use app\models\PasteForm;
use app\models\PasteStatus;
use app\models\Comment;
use app\models\CommentForm;
use app\models\CommentLike;
use app\models\Highlighting;
use app\models\Setting;
use yii\helpers\ArrayHelper;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($id = null)
    {
        if ($id==null)
        {
            $model = new PasteForm();
            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->setFlash('pasteFormSubmitted');
                $paste = $model->getLastModel();            
                return $this->redirect(Url::toRoute('/paste/'. $paste->id));
            }
            
            $highlighting = ArrayHelper::map(Highlighting::find()->all(), 'highlighting_id', 'name');
            $status = ArrayHelper::map(PasteStatus::find()->all(), 'pastestatus_id', 'name');
            $tags = ArrayHelper::map(PasteStatus::find()->all(), 'pastestatus_id', 'name');
            $users = ArrayHelper::map(User::find()->all(), 'user_id', 'user_name');
            
            //Not logged user can not create draft pastes
            if (Yii::$app->user->isGuest) {
                unset($status[1]);
            }
            
            return $this->render('index', [
                'isAvailable' => Setting::getValue('GUEST_CREATE_PASTE'),
                'model' => $model,
                'highlighting' => $highlighting,
                'status' => $status,
                'tags' => $tags,
                'users' => $users,
            ]);
        } 
        else 
        {
            $commentForm = new CommentForm();
            if (Yii::$app->request->isPost)
            {
                if (isset($_POST['comment-like-button']))
                {
                    if (isset(Yii::$app->user->identity)) {
                        $user = Yii::$app->user->identity;
                        $like = new CommentLike();
                        $like->comment_like_comment_id = $_POST['comment_id'];
                        $like->comment_like_user_id = $user->getId();
                        $like->save();
                        return 1;
                    }                
                }
                if (isset($_POST['comment-unlike-button']))
                {
                    if (isset(Yii::$app->user->identity)) {
                        $user = Yii::$app->user->identity;
                        if ($like = CommentLike::findByUsersIds($user->getId(), $_POST['comment_id'] ) )
                        $like->delete();   
                        
                        return 1;
                    }                
                }
                if (isset($_POST['comment-button'])) 
                {
                    $commentForm->author_id = Yii::$app->user->identity->user_id;
                    $commentForm->paste_id = $id;
                    $commentForm->status = Setting::getValue('PASTE_COMMENT_DEFAULT_STATUS');;
                    if ($commentForm->load(Yii::$app->request->post()) && $commentForm->save()) {
                        Yii::$app->session->setFlash('commentFormSubmitted');
                    }
                }
                
            }            
            if (\Yii::$app->user->isGuest) {
                $model = Paste::findPublishedById($id);
            } else {
                $model = Paste::findById($id);
                if ($model->user_id!=Yii::$app->user->identity->user_id && $model->status_id!=Paste::STATUS_PUBLISHED) {
                    throw new  \yii\web\NotFoundHttpException('The specified post cannot be found.');

                }
            }
            if ($model) 
            {
                $model->views++;
                $model->save();
                //echo $id;die();
                $comments = Comment::findByPastesId($id);
                
                if (Yii::$app->request->isPost)
                {
                    echo 1;
                } else {
                    return $this->render('paste', [
                        'model' => $model,
                        'commentForm' => $commentForm,
                        'comments' => $comments,
                    ]);
                }
            }
            else {
                throw new \yii\web\NotFoundHttpException('This page is no longer available.');
            }
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    { 
        if (!\Yii::$app->user->isGuest) {
                return $this->goHome();
        }

        $db = \Yii::$app->db;
        $loginModel = new LoginForm();
        

        if (Yii::$app->request->isPost)
        {
            if (isset($_POST['login-button']))
            {
                if ($loginModel->load(Yii::$app->request->post()) && $loginModel->login())
                {
                    return $this->goBack();
                }
                else {
                    return $this->render('login', [
                        'model' => $loginModel,
                    ]);
                }
            }
                
        }
        return $this->render('login', [
                'model' => $loginModel,
                //'registerModel' => $registerModel
        ]);
}
    
    /**
     * Check after FbLogin.
     *
     * @return string
     */
    public function actionFblogin()
    {
        //session_start();

        $fb = new \Facebook\Facebook([
                'app_id' => '960989454004531',
                'app_secret' => '4d18ed2c60fec150cebff249c9919952',
                'default_graph_version' => 'v2.3',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        try
        {
                $accessToken = $helper->getAccessToken();
        }
        catch (\Exception $e)
        {
                return $this->redirect(Url::toRoute('/login'));
        }

        try
        {
                $response = $fb->get('/me?fields=id,name,email', $accessToken);
        }
        catch (\Exception $e)
        {
                return $this->redirect(Url::toRoute('/login'));
        }

        $fbUser = $response->getGraphUser();

        if (!$user = User::findByFacebookId($fbUser->getId()))
        {
            $user = new User();
            $fbUserName = explode(' ', $fbUser->getName());
            
            if (count($fbUserName)>1) {
                $user->user_first_name = $fbUserName[0];
                $user->user_last_name = $fbUserName[1];
            }
            $user->user_name        = strtolower( str_replace(' ','', $fbUser->getName() ) );
            $user->user_facebook_id = $fbUser->getId();            
            $user->user_email       = $fbUser->getField('email');
            $user->user_status      = 1;
            $user->user_photo       = 'http://graph.facebook.com/'.$fbUser->getId().'/picture?type=square';

            $user->save();
        }
        
        Yii::$app->user->login($user, 0);
        
        $user->user_last_login_date = date('Y-m-d H:i:s');
        $user->save();
        
        return $this->redirect('/');
    }
    
    /**
     * Check after GgLogin.
     *
     * @return string
     */
    public function actionGglogin()
    {        
        if (!isset($_GET['code'])) {
            return $this->redirect(Url::toRoute('/login'));
        }
        
        $client = new \Google_Client();
        $client->setClientId(Yii::$app->params['GClientID']);
        $client->setClientSecret(Yii::$app->params['GClientSecret']);
        $client->setRedirectUri('http://www.pastebin.com.pl/gglogin');
        $client->setScopes(['email']);
        //$client->setAccessType('offline');
        
        $plus = new \Google_Service_Plus($client);
        
        try
        {
            $client->authenticate($_GET['code']);
            $accessToken = $client->getAccessToken();
        }
        catch (\Exception $e)
        {
            return $this->redirect(Url::toRoute('/login'));
        }
        
        try
        {
            $client->setAccessToken($accessToken);
            $me = $plus->people->get('me');
        }
        catch (\Exception $e)
        {
            return $this->redirect(Url::toRoute('/login'));
        }
        
        if (!$user = User::findByGoogleId($me['id']))
        {
            $user = new User();
            $ggUserName = explode(' ', $me['displayName']);
            
            if (count($ggUserName)>1) {
                $user->user_first_name = $ggUserName[0];
                $user->user_last_name = $ggUserName[1];
            }
            $user->user_name = strtolower( str_replace(' ','', $me['displayName'] ) );
            $user->user_googleplus_id = $me['id'];
            $user->user_email = $me['emails'][0]['value'];
            $user->user_photo = $me['image']['url'];
            $user->user_status = 1;

            $user->save();
        }
        
        Yii::$app->user->login($user, 0);
        
        $user->user_last_login_date = date('Y-m-d H:i:s');
        $user->save();
            
        return $this->redirect('/');
    }
    
    
    /**
     * Check after TwLogin.
     *
     * @return string
     */
    public function actionTwlogin()
    {        
        if (!isset($_REQUEST['oauth_verifier'], $_REQUEST['oauth_token']) && !$_REQUEST['oauth_token'] ) {
            return $this->redirect(Url::toRoute('/login'));
        }
        
        $consumer_key = Setting::getValue('AUTH_TWITTER_API_CONSUMER_KEY');
        $consumer_secret = Setting::getValue('AUTH_TWITTER_API_CONSUMER_SECRET');

        $connection = new \Abraham\TwitterOAuth\TwitterOAuth($consumer_key, $consumer_secret);

        //necessary to get access token other wise u will not have permision to get user info
        $params=["oauth_verifier" => $_GET['oauth_verifier'],"oauth_token"=>$_GET['oauth_token'] ];
        $access_token = $connection->oauth("oauth/access_token", $params);

        //now again create new instance using updated return oauth_token and oauth_token_secret because old one expired if u dont u this u will also get token expired error
        $connection = new \Abraham\TwitterOAuth\TwitterOAuth($consumer_key, $consumer_secret,
        $access_token['oauth_token'],$access_token['oauth_token_secret']);

        $content = $connection->get("account/verify_credentials");
        
        
       
        /*
        try
        {
            $client->authenticate($_GET['code']);
            $accessToken = $client->getAccessToken();
        }
        catch (\Exception $e)
        {
            return $this->redirect(Url::toRoute('/login'));
        }
        
        try
        {
            $client->setAccessToken($accessToken);
            $me = $plus->people->get('me');
        }
        catch (\Exception $e)
        {
            return $this->redirect(Url::toRoute('/login'));
        }
         */
        if (!$user = User::findByTwitterId($content->id))
        {   
            
            $user = new User();
            $twUserName = explode(' ', $content->name);
            
            if (count($ggUserName)>1) {
                $user->user_first_name = $twUserName[0];
                $user->user_last_name = $twUserName[1];
            }
            $user->user_name = strtolower( str_replace(' ','', $content->screen_name ) );
            $user->user_twitter_id = $content->id;
            $user->user_email = '';//app have been on whitelist to get this :(
            $user->user_photo = $content->profile_image_url_https;
            $user->user_status = 1;

            $user->save();
        }
        
        Yii::$app->user->login($user, 0);
        
        $user->user_last_login_date = date('Y-m-d H:i:s');
        $user->save();
            
        return $this->redirect('/');
    }
    
    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
    
    /**
     * Registration action.
     *
     * @return string
     */
    public function actionRegistration()
    {
        if (!\Yii::$app->user->isGuest) {
                return $this->goHome();
        }

        $db = \Yii::$app->db;
        $registerModel = new RegisterForm();        

        if (Yii::$app->request->isPost)
        {
            if (isset($_POST['register-button']))
            {
                if ($registerModel->load(Yii::$app->request->post()) && ($user = $registerModel->register()))
                {
                    Yii::$app->session->setFlash('profile_created', 'Your account has been created.');

                    return $this->redirect(Url::toRoute('/registration'));
                }
            }                
        }
        return $this->render('registration', [
                'registerModel' => $registerModel
        ]);
    }
    /**
     * Displays contact page.
     *
     * @return string
     */
//    public function actionContact()
//    {
//        $model = new ContactForm();
//        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
//            Yii::$app->session->setFlash('contactFormSubmitted');
//
//            return $this->refresh();
//        }
//        return $this->render('contact', [
//            'model' => $model,
//        ]);
//    }
    
    /**
     * Displays trends page.
     *
     * @return string
     */
    public function actionTrends()
    {
        $PastesList = Paste::find()->where('status_id = :status', ['status'=>Paste::STATUS_PUBLISHED])->limit(100)->all();
        
        return $this->render('trends', [
            'pastesList' => $PastesList,
        ]);
    }
    
    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    /**
     * Displays terms page.
     *
     * @return string
     */
    public function actionTerms()
    {
        return $this->render('terms');
    }
    
    /**
     * Displays terms page.
     *
     * @return string
     */
    public function actionPrivacy()
    {
        return $this->render('privacy');
    }
    
    /**
     * Generating sitemap.
     *
     * @return string
     */
    public function actionSitemap()
    {
        $domain = Yii::$app->params['domainName'];
        $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"/>');
        $url = $xml->addChild('url');
        $url->addChild('loc', $domain.Url::toRoute('/login'));
        $url->addChild('lastmod', date('Y-m-d'));
        $url->addChild('changefreq', 'daily');
        
        $url = $xml->addChild('url');
        $url->addChild('loc', $domain.Url::toRoute('/registration'));
        $url->addChild('lastmod', date('Y-m-d'));
        $url->addChild('changefreq', 'daily');
        
        $url = $xml->addChild('url');
        $url->addChild('loc', $domain.Url::toRoute('/trends'));
        $url->addChild('lastmod', date('Y-m-d'));
        $url->addChild('changefreq', 'daily');
        
        $url = $xml->addChild('url');
        $url->addChild('loc', $domain.Url::toRoute('/about'));
        $url->addChild('lastmod', date('Y-m-d'));
        $url->addChild('changefreq', 'daily');
        
        $url = $xml->addChild('url');
        $url->addChild('loc', $domain.Url::toRoute('/terms'));
        $url->addChild('lastmod', date('Y-m-d'));
        $url->addChild('changefreq', 'daily');
        
        $url = $xml->addChild('url');
        $url->addChild('loc', $domain.Url::toRoute('/privacy'));
        $url->addChild('lastmod', date('Y-m-d'));
        $url->addChild('changefreq', 'daily');
        
        $users = User::find()->all();
        foreach ($users as $user) {
            $url = $xml->addChild('url');
            $url->addChild('loc', $domain.Url::toRoute('/user/' . $user->user_name ) );
            $url->addChild('lastmod', date('Y-m-d'));
            $url->addChild('changefreq', 'hourly');
        }
        
        $pastes = Paste::find()->where('status_id = :status', ['status'=>Paste::STATUS_PUBLISHED])->all();
        foreach ($pastes as $paste) {
            $url = $xml->addChild('url');
            $url->addChild('loc', $domain.Url::toRoute('/paste/' . $paste->getId() ) );
            $url->addChild('lastmod', date('Y-m-d', $paste->modification_date));
            $url->addChild('changefreq', 'hourly');
            //echo '<pre>',print_r($paste,1);die();
        }
        
        Header('Content-type: text/xml');
        print($xml->asXML());
    }
    
    public function actionForgotten()
    {
        if (!\Yii::$app->user->isGuest) {
                return $this->goHome();
        }
        $model = new ForgottenForm();
        
        if (Yii::$app->request->isPost)
        {
            $post = Yii::$app->request->post();
            if ($model->load(Yii::$app->request->post()) && $model->send($post['ForgottenForm']['email'])) {
                Yii::$app->session->setFlash('forgotten_submitted');
                return $this->redirect(Url::toRoute('/'));
            }
        }
        return $this->render('forgotten', [
                'model' => $model,
        ]);
    }
}
