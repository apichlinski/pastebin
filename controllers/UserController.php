<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use app\models\User;
use app\models\UserProfileForm;
use app\models\Paste;
use app\models\Follow;
use app\models\Comment;

class UserController extends Controller
{
    public function actionIndex($id)
    {
        if (Yii::$app->request->isAjax)
        {
            if (@$_POST['name'] == 'profileImage')
            {
                $db = Yii::$app->db;
                $photoID = (int)@$_POST['value'];
                $update = true;
                $image = null;

                if ($photoID)
                {
                    if (!($image = $db->createCommand('SELECT * FROM plik WHERE plik_id = :plik_id AND plik_user_id = :user_id')
                            ->bindValue(':plik_id', $photoID)
                            ->bindValue(':user_id', Yii::$app->user->identity->getId())
                            ->queryOne()))
                    {
                        $update = false;
                    }
                }

                if ($update)
                {
                    $db->createCommand('UPDATE user SET user_photo_id = :zdjecie_id WHERE user_id = :id')
                            ->bindValue(':id', Yii::$app->user->identity->getId())
                            ->bindValue(':zdjecie_id', $photoID ? $photoID : null)
                            ->execute();
                }

                if (!$photoID)
                {
                    echo 'none';
                }
                else
                {
                    if ($image)
                    {
                        echo 'url(' . HtmlHelper::getImagePath($image, 200, 200) . ')';
                    }
                }
                exit;
            }
        }
        if ($model = User::findByUsername($id)) {
            $formModel = new UserProfileForm();

            if (isset(Yii::$app->user->identity)) {
                $user = Yii::$app->user->identity;

                $formModel->firstname = $user->user_first_name;
                $formModel->lastname = $user->user_last_name;
                $formModel->username = $user->user_name;
                $formModel->email = $user->user_email;
                $formModel->sex = $user->user_sex;
                $formModel->education = $user->user_education;
                $formModel->location = $user->user_location;
                $formModel->skills = $user->user_skills;
                $formModel->about = $user->user_description;
            }
            if (Yii::$app->request->isPost)
            {
                if (isset($_POST['follow-button']))
                {
                    if (isset(Yii::$app->user->identity)) {
                        $user = Yii::$app->user->identity;
                        $follow = new Follow;
                        $follow->follow_user_id = $user->getId();
                        $follow->follow_followed_user_id = $model->getId();
                        $follow->save();
                    }                
                }
                if (isset($_POST['unfollow-button']))
                {
                    if (isset(Yii::$app->user->identity)) {
                        $user = Yii::$app->user->identity;
                        if ($follow = Follow::findByUsersIds($user->getId(), $model->getId()) )
                        $follow->delete();                        
                    }                
                }

                if (isset($_POST['save-button']))
                {
                    if (isset(Yii::$app->user->identity)) {
                        $formModel->load(Yii::$app->request->post());

                        if ($formModel->load(Yii::$app->request->post()))
                        {
                            $user = Yii::$app->user->identity;
                            $user->user_first_name = $formModel->firstname;
                            $user->user_last_name = $formModel->lastname;
                            //$user->user_email = $this->email;
                            $user->user_sex = $formModel->sex;
                            //$user->user_verification_code = md5(uniqid(microtime(), true));
                            //$user->user_verification_date = date('Y-m-d H:i:s');                        
                            $user->user_education = $formModel->education;
                            $user->user_location = $formModel->location;
                            $user->user_skills = $formModel->skills;
                            $user->user_description = $formModel->about;
                            
                            if ($user->save() ){
                                Yii::$app->session->setFlash('profile_saved', 'Changes have been saved.');
                            }

                            return $this->redirect(Url::toRoute('/user/'.$model->user_name.'?tab=settings'));
                        }
                    }
                }
            }
            $pastes = Paste::find()->where('status_id = :status and user_id = :user_id', ['status'=>Paste::STATUS_PUBLISHED, 'user_id' => $model->getId()] )->all();
            $comments = Comment::find()->where('comment_status = :status and comment_author_id = :user_id', ['status'=>1, 'user_id' => $model->getId()] )->all();
            
            $followers = Follow::find()
                    ->select('`follow`.*')
                    ->leftJoin('user', '`user`.`user_id` = `follow`.`follow_user_id`')
                    ->where('follow_followed_user_id = :user_id', ['user_id' => $model->getId()] )
                    ->orderBy('follow_date DESC')
                    ->all();
            
            $following = Follow::find()
                    ->select('`follow`.*')
                    ->leftJoin('user', '`user`.`user_id` = `follow`.`follow_followed_user_id`')
                    ->where('follow_user_id = :user_id', ['user_id' => $model->getId()] )
                    ->orderBy('follow_date DESC')
                    ->all();
            
            
            $get = Yii::$app->request->get();
            $defaultTabs = array('active','comments','followers','following','avatar','settings');
            $activeTab = isset($get['tab'])?$get['tab']:'active';
            if (!in_array($activeTab, $defaultTabs)) {
                $activeTab = 'active';
            }
            
            return $this->render('index', [
                'model'     => $model,
                'formModel' => $formModel,
                'pastes'    => $pastes,
                'comments'  => $comments,
                'followers' => $followers,
                'following' => $following,
                'tab'       => $activeTab,
                //'expertTypes' => $expertTypes,
                //'mailSent' => $mailSent
            ]);
        } else {
             throw new  \yii\web\NotFoundHttpException('The specified user cannot be found.');
        }        
        
    }
}