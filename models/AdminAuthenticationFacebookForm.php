<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * PasteForm is the model behind the paste form.
 */
class AdminAuthenticationFacebookForm extends Model
{
    public $active;
    public $app_id;
    public $app_secret;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['app_id', 'app_secret'], 'safe'],
            [['active'], 'boolean'],
            [['app_id', 'app_secret'], 'string'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'active' => 'Active',
            'app_id' => 'App ID',
            'app_secret' => 'App Secret',
        ];
    }
    
    public function save()
    {
        if ($this->validate())
        {
            Setting::setValue('AUTH_FACEBOOK_API_ACTIVE', $this->active, '1');
            Setting::setValue('AUTH_FACEBOOK_API_APP_ID', $this->app_id, '1');
            Setting::setValue('AUTH_FACEBOOK_API_APP_SECRET', $this->app_secret, '1');
            
            return true;
        }
        else
        {
            return false;
        }
    }
}