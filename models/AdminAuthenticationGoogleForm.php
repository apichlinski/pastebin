<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * PasteForm is the model behind the paste form.
 */
class AdminAuthenticationGoogleForm extends Model
{
    public $active;
    public $client_id;
    public $client_secret;
   
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['client_id', 'client_secret'], 'safe'],
            [['active'], 'boolean'],
            [['client_id', 'client_secret'], 'string'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'active' => 'Active',
            'client_id' => 'Client ID',
            'client_secret' => 'Client Secret',
        ];
    }
        
    public function save()
    {
        if ($this->validate())
        {
            Setting::setValue('AUTH_GOOGLE_API_ACTIVE', $this->active, '1');
            Setting::setValue('AUTH_GOOGLE_API_CLIENT_ID', $this->client_id, '1');
            Setting::setValue('AUTH_GOOGLE_API_CLIENT_SECRET', $this->client_secret, '1');
            
            return true;
        }
        else
        {
            return false;
        }
    }
}