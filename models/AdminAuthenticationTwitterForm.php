<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * PasteForm is the model behind the paste form.
 */
class AdminAuthenticationTwitterForm extends Model
{
    public $active;
    public $api_key;
    public $api_secret;
   
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['api_key', 'api_secret'], 'safe'],
            [['active'], 'boolean'],
            [['api_key', 'api_secret'], 'string'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'active' => 'Active',
            'api_key' => 'API Key',
            'api_secret' => 'API Secret',
        ];
    }
        
    public function save()
    {
        if ($this->validate())
        {
            Setting::setValue('AUTH_TWITTER_API_ACTIVE', $this->active, '1');
            Setting::setValue('AUTH_TWITTER_API_CONSUMER_KEY', $this->api_key, '1');
            Setting::setValue('AUTH_TWITTER_API_CONSUMER_SECRET', $this->api_secret, '1');
            
            return true;
        }
        else
        {
            return false;
        }
    }
}