<?php

namespace app\models;

use Yii;
use yii\base\Model;

class AdminSettingsForm extends Model
{
    public $login_required;
    public $email_confirmation_required;

    public function rules()
    {
        return [
            ['login_required', 'boolean'],
            ['email_confirmation_required', 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'login_required' => 'Only registered users can create pastes',
            'email_confirmation_required' => 'Email address confirmation required'
        ];
    }
    
    public function save($id = null)
    {
        if ($this->validate())
        {
            /*
            $user = $id ? User::findIdentity($id) : new User();
            
            $user->login_required = $this->login_required;
            $user->user_name = $this->username;
            $user->user_first_name = $this->firstname;
            $user->user_last_name = $this->lastname;
            $user->user_email = $this->email;
            $user->user_sex = $this->sex;
            //$user->user_verification_code = md5(uniqid(microtime(), true));
            //$user->user_verification_date = date('Y-m-d H:i:s');                        
            $user->user_description = $this->about;
            $user->user_administrator_type_id = $this->userAdminType;

            if ($id )
            {   
                $this->_user = $user;
                return $user->update() !== false;
            }
            else
            {
                $user->user_create_date = date('Y-m-d H:i:s');
                $this->_user = $user;
                return $user->save();
            }
             */
        }
        else
        {
            return false;
        }
    }
}
