<?php

namespace app\models;

use Yii;
use yii\base\Model;

class AdminUserForm extends Model
{
    public $status;
    public $username;
    public $password;
    public $password_repeat;
    public $userType;
    public $userAdminType;
    public $sex;
    public $lastname;
    public $firstname;
    public $email;
    public $about;
    
    private $_user;

    public function rules()
    {
        return [
            ['username', 'required', 'message' => 'Plase enter username'],
            //['username', 'validateUsername'],
            //['password', 'required', 'message' => 'Podaj hasło'],
            ['status', 'boolean'],
            ['password', 'compare', 'message' => 'Podane hasła są różne'],
            //['about' => 'safe'],
            //['firstname', 'required', 'message' => 'Plase enter first name'],
            //['lastname', 'required', 'message' => 'Plase enter last name'],
            //['sex', 'required', 'message' => 'Plase enter sex'],
            [['sex', 'about', 'lastname', 'firstname','password','password_repeat'], 'string'],
            [['userType', 'userAdminType'], 'integer'],
            //['password', 'validatePassword'],
            //['password', 'compare', 'message' => 'Podane hasła są różne'],
            //['password_repeat', 'required', 'message' => 'Powtórz hasło'],
            ['email', 'required'],
            ['email', 'email', 'message' => 'Plase enter valid email address'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'status' => 'Active',
            'username' => 'Username',
            'password' => 'Password',
            'password_repeat' => 'Repeat password',
            'userAdminType' => 'Account type',
            'sex' => 'Sex',
            'firstname' => 'First name',
            'lastname' => 'Last name',
            'email' => 'E-mail address',
        ];
    }

    public function validateUsername($attribute, $params)
    {
        if (!$this->hasErrors())
        {
            $db = Yii::$app->db;

            if ($user = $db->createCommand('SELECT * FROM user WHERE user_name = :user_name')->bindValue(':user_name', $this->username)->queryOne())
            {
                $this->addError($attribute, 'Użytkownik o podanej nazwie jest już zarejestrowany');
            }
        }
    }

    public function validatePassword($attribute, $params)
    {
        return;

        if (!$this->hasErrors())
        {
            $user = $this->getUser();

            try
            {
                if (!$user || !$user->validatePassword($this->password)) {
                    $this->addError($attribute, 'Niewłaściwa nazwa użytkownika lub hasło.');
                }
            }
            catch (\Exception $e)
            {
                $this->addError($attribute, 'Niewłaściwa nazwa użytkownika lub hasło.');
            }
        }
    }
        
    public function getLastModel() 
    {
        return $this->_user;
    }
    
    public function save($id = null)
    {
        if ($this->validate())
        {
            $user = $id ? User::findIdentity($id) : new User();
            
            $user->user_status = $this->status;
            $user->user_name = $this->username;
            $user->user_first_name = $this->firstname;
            $user->user_last_name = $this->lastname;
            $user->user_email = $this->email;
            $user->user_sex = $this->sex;
            //$user->user_verification_code = md5(uniqid(microtime(), true));
            //$user->user_verification_date = date('Y-m-d H:i:s');                        
            $user->user_description = $this->about;
            $user->user_administrator_type_id = $this->userAdminType;

            if ($this->password!=''){
                $user->setPassword($this->password);
            }

            if ($id )
            {   
                $this->_user = $user;
                return $user->update() !== false;
            }
            else
            {
                $user->user_create_date = date('Y-m-d H:i:s');
                $this->_user = $user;
                return $user->save();
            }
        }
        else
        {
            return false;
        }
    }
}
