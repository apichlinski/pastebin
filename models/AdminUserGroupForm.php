<?php

namespace app\models;

use Yii;
use yii\base\Model;

class AdminUserGroupForm extends Model
{
	public $name;
	public $access_matrix;

	public function rules()
	{
		return [
                        ['name', 'required', 'message' => 'Podaj nazwę grupy.'],
                        ['name', 'string'],
		];
	}
        
        public function save($userGroupId = null)
	{
            if ($this->validate())
            {
                $userGroup = $userGroupId ? UserGroup::findOne($userGroupId) : new UserGroup(); 
                $userGroup->user_group_name = $this->name;
                if ($userGroupId)
                {
                        return $userGroup->update() !== false;
                }
                else
                {
                        return $userGroup->save();
                }
            }
            else
            {
                return false;
            }
	}
}
