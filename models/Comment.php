<?php

namespace app\models;

use yii\db\ActiveRecord;

class Comment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->comment_id;
    }
    
    public static function findByCommentByUserId($follow_user_id, $follow_followed_user_id)
    {
        //return static::findOne(['follow_user_id' => $follow_user_id, 'follow_followed_user_id' => $follow_followed_user_id]);
    }
    
    public static function findByPastesId($paste_id)
    {
        //return static::find(['comment_author_id' => $user_id, 'comment_status' => 1 ])->orderBy('comment_creation_date')->all();
        return static::find()->where(['comment_paste_id' => $paste_id, 'comment_status' => 1 ])->orderBy('comment_creation_date')->all();
    }
    
    public function getAuthor()
    {
        return $this->comment_author_id!=0?$this->hasOne(User::className(), ['user_id' => 'comment_author_id']):false;
    }
    
    public static function countAllComments()
    {
        return (int)$countComments = Comment::find()
        //->where('approved = 1')
        //->groupBy(['promoter_location_id', 'lead_type_id'])
        ->count();
    }
    
    public function countLikes()
    {
        return (int)$countLikes = CommentLike::find()
            ->where(['comment_like_comment_id' => $this->getId()])
            //->groupBy(['promoter_location_id', 'lead_type_id'])
            ->count();
    }
    
    public function getPasteUrl()
    {
        return '/paste/'.$this->comment_paste_id;
    }
}
