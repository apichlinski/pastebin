<?php

namespace app\models;

use Yii;
use yii\base\Model;

class CommentForm extends Model
{
    public $author_id;
    public $paste_id;
    public $status;
    //public $title;
    public $content;

    public function rules()
    {
        return [
            ['paste_id', 'string'],
            ['author_id', 'integer'],
            //['title', 'required', 'message' => 'Plase enter comment title'],
            ['status', 'boolean'],
            //['password', 'validatePassword'],
            //['password', 'compare', 'message' => 'Podane hasła są różne'],
            //['password_repeat', 'required', 'message' => 'Powtórz hasło'],
            [['paste_id', 'author_id', 'content'], 'required'],
            //['email', 'email', 'message' => 'Plase enter valid email address'],
        ];
    }
    /*
    public function attributeLabels()
    {
        return [
            'status' => 'Active',
            'username' => 'Username',
            'password' => 'Password',
            'password_repeat' => 'Repeat password',
            'userAdminType' => 'Account type',
            'sex' => 'Sex',
            'firstname' => 'First name',
            'lastname' => 'Last name',
            'email' => 'E-mail address',
        ];
    }
    */
    public function save($id = null)
    {
        if ($this->validate())
        {
            $comment = $id ? Comment::findIdentity($id) : new Comment();
            
            $comment->comment_status = $this->status;
            $comment->comment_paste_id = $this->paste_id;
            //$comment->comment_title = $this->title;
            $comment->comment_content = $this->content;
            $comment->comment_author_id = $this->author_id;

            if ($id )
            {                   
                $comment->comment_update_date = date('Y-m-d H:i:s');
                return $comment->update() !== false;
            }
            else
            {
                $comment->comment_author_id = $this->author_id;
                $comment->comment_creation_date = date('Y-m-d H:i:s');
                return $comment->save();
            }
            
        }
        else
        {
            return false;
        }
    }
}
