<?php

namespace app\models;

use yii\db\ActiveRecord;

class CommentLike extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }
    
    public static function findByUsersIds($like_user_id, $comment_id)
    {
        return static::findOne(['comment_like_user_id' => $like_user_id, 'comment_like_comment_id' => $comment_id]);
    }
}
