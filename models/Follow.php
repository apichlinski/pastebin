<?php

namespace app\models;

use yii\db\ActiveRecord;

class Follow extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }
    
    public static function findByUsersIds($follow_user_id, $follow_followed_user_id)
    {
        return static::findOne(['follow_user_id' => $follow_user_id, 'follow_followed_user_id' => $follow_followed_user_id]);
    }
    
    public function getFollower()
    {
        return $this->follow_user_id!=0?$this->hasOne(User::className(), ['user_id' => 'follow_user_id']):false;
    }
    
    public function getFollowed()
    {
        return $this->follow_followed_user_id!=0?$this->hasOne(User::className(), ['user_id' => 'follow_followed_user_id']):false;
    }
}
