<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\components\L;

/**
 * PasteForm is the model behind the paste form.
 */
class ForgottenForm extends Model
{
    public $email;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            ['email', 'required'],
            ['email', 'safe'],
//            [['email'], 'safe'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            //'verifyCode' => 'Verification Code',
        ];
    }
    
    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return boolean whether the model passes validation
     */
    public function send($email)
    {
        if ($this->validate()) {
            $body = 'There is your new password: ';
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$this->email => $this->email])
                ->setSubject('New password')
                ->setTextBody($body)
                ->send();

            return true;
        }
        return false;
    }
}