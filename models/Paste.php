<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\Url;   
use app\models\User;
use app\models\Highlighting;

class Paste extends ActiveRecord
{
    const STATUS_DRAFT=1;
    const STATUS_PUBLISHED=2;
    const STATUS_ARCHIVED=3;
    
    public static function tableName()
    {
        return 'paste';
    }
    
    /**
     * Finds paste by id
     *
     * @param string $id
     * @return static|null
     */
    public static function findById($id)
    {
        //return Paste::where('id != :id and ( status = :status OR user_id = :user_id)', ['id'=>$id, 'status'=>1])->all();
        return Paste::find()->where('id = :id', ['id'=>$id])->one();
    }
    
    
    /**
     * Finds paste by id
     *
     * @param string $id
     * @return static|null
     */
    public static function findPublishedById($id)
    {
        //return Paste::where('id != :id and ( status = :status OR user_id = :user_id)', ['id'=>$id, 'status'=>1])->all();
        return Paste::find()->where('id = :id AND status_id = :status', ['id'=>$id,'status'=>self::STATUS_PUBLISHED])->one();
    }
    
    /**
     * Finds recently added pastes
     *
     * @param string $limit
     * @return static|null
     */
    public static function findRecentlyAdded($limit)
    {
        return Paste::find()->where('status_id = :status AND allowed_users IS NULL AND active_to IS NULL AND active_to IS NULL', ['status'=>self::STATUS_PUBLISHED])->limit($limit)->orderBy('insert_date DESC')->all();
    }
    
    /**
     * Finds paste by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUserId($userId)
    {
        /*
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;*/
        return Paste::find()->where('user_id = :id AND status_id = :status', ['id'=>$userId,'status'=>self::STATUS_PUBLISHED])->all();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    public function getAuthor()
    {
        return $this->user_id!=0?$this->hasOne(User::className(), ['user_id' => 'user_id']):false;
    }
    
    public function getHighlighting()
    {
        return $this->hasOne(Highlighting::className(), ['highlighting_id' => 'highlighting_id']);
    }
    
    public function getPasteStatus()
    {
        return $this->hasOne(Highlighting::className(), ['status_id' => 'pastestatus_id']);
    }
    
    public static function countAllPastes()
    {
        return (int)Paste::find()
        //->select(['COUNT(*) AS cnt'])
        //->where('approved = 1')
        //->groupBy(['promoter_location_id', 'lead_type_id'])
        ->count();
    }    
    
    public function getPasteUrl()
    {
        return Url::toRoute('/paste/' . $this->getId() );
    }
}
