<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\components\L;

/**
 * PasteForm is the model behind the paste form.
 */
class PasteForm extends Model
{
    public $title;
    public $highlighting_id;
    public $content;
    public $tags;
    public $status_id;
    public $active_from;
    public $active_to;
    public $insert_date;
    public $modification_date;
    public $allowed_users;
    public $views;
    public $verifyCode;
    public $user_id;
    
    private $_paste;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // title, highlighting, content are required
            [['highlighting_id', 'content'], 'required'],
            // title, highlighting, content and tags are safe
            [['title', 'highlighting_id', 'content', 'tags'], 'safe'],
            // verifyCode needs to be entered correctly
            ['verifyCode', 'captcha'],
            //highlighting_id, status and user_id are integer
            [['highlighting_id', 'status_id', 'user_id'], 'integer'],
            //allowed_users, are string
            //[['allowed_users'], 'arrayOfInt','allowEmpty'=>false],
            //['allowed_users', 'in', 'range' => [2, 1, 6, 8], 'allowArray' => true],
            ['allowed_users', 'each', 'rule' => ['in', 'range' => [User::getAllIds()], 'allowArray' => true]],
            //active_from, active_to are date
            [['active_from','active_to'], 'date', 'format' => 'yyyy-M-d', /*'message' => 'Nieprawidłowa data'*/],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            //'verifyCode' => 'Verification Code',
        ];
    }
    
    public function getLastModel() 
    {
        return $this->_paste;
    }
    
    public function save($id = null)
    {
        if ($this->validate())
        {
            $paste = $id ? Paste::findOne($id) : new Paste();
            if (!$id) {
                $paste->id          = L::unique_id();
            }
            $paste->title           = $this->title!=''?$this->title:'Untitled';
            $paste->content         = $this->content;
            $paste->highlighting_id = $this->highlighting_id;
            $paste->tags            = $this->tags;
            $paste->status_id       = $this->status_id;
            $paste->active_from     = $this->active_from;
            $paste->active_to       = $this->active_to;
            $paste->allowed_users   = $this->allowed_users;            
            $paste->views           = 0;
            
            if (Yii::$app->user->isGuest)
                $paste->user_id = 0;
            else
                $paste->user_id = Yii::$app->user->identity->getId();
                
            if ($id)
            {                
                if (Yii::$app->user->isGuest)
                    $paste->modification_user_id = 0;
                else
                    $paste->modification_user_id = Yii::$app->user->identity->getId();
                
                $paste->modification_date = date('Y-m-d H:i:s');
                $this->_paste = $paste;
                return $paste->update() !== false;
            }
            else
            {
                $paste->insert_date = date('Y-m-d H:i:s');
                $this->_paste = $paste;
                return $paste->save();
            }
        }
        else
        {
            return false;
        }
    }
}