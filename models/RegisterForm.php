<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Url;

class RegisterForm extends Model
{
	public $username;
	public $password;
	public $password_repeat;
	public $userType;
	public $sex;
	public $lastname;
	public $firstname;
	public $email;
        public $agreement;

	public function rules()
	{
		return [
			['username', 'required', 'message' => 'Podaj nazwę użytkownika'],
			['username', 'validateUsername'],
			['password', 'required', 'message' => 'Podaj hasło'],
			#['firstname', 'required', 'message' => 'Podaj imię'],
			#['lastname', 'required', 'message' => 'Podaj nazwisko'],
			#['sex', 'required', 'message' => 'Podaj płeć'],
			['password', 'validatePassword'],
			['password', 'compare', 'message' => 'Podane hasła są różne'],
			['password_repeat', 'required', 'message' => 'Powtórz hasło'],
			['email', 'required'],
			['email', 'email', 'message' => 'Podaj porawny adres e-mail'],
                        ['agreement', 'required', 'requiredValue' => 1, 'message' => 'You should accept our Term and Conditions to use our service'],
		];
	}

	public function attributeLabels()
	{
		return [
			'username' => 'Nazwa użytkownika',
			'password' => 'Hasło',
			'password_repeat' => 'Powtórz hasło',
			'userType' => 'Rodzaj konta',
			'expertType' => 'Specjalizacja',
			'sex' => 'Płeć',
			'firstname' => 'Imię',
			'lastname' => 'Nazwisko',
			'email' => 'Adres e-mail',
                        'agreement' => 'I agree to the terms',
		];
	}

	public function validateUsername($attribute, $params)
	{
            if (!$this->hasErrors())
            {
                $db = Yii::$app->db;

                if ($user = $db->createCommand('SELECT * FROM user WHERE user_name = :user_name')->bindValue(':user_name', $this->username)->queryOne())
                {
                    $this->addError($attribute, 'Użytkownik o podanej nazwie jest już zarejestrowany');
                }
            }
	}
	
	public function validatePassword($attribute, $params)
	{
            return;

            if (!$this->hasErrors())
            {
                $user = $this->getUser();

                try
                {
                    if (!$user || !$user->validatePassword($this->password)) {
                        $this->addError($attribute, 'Niewłaściwa nazwa użytkownika lub hasło.');
                    }
                }
                catch (\Exception $e)
                {
                    $this->addError($attribute, 'Niewłaściwa nazwa użytkownika lub hasło.');
                }
            }
	}

	public function register()
	{
            if ($this->validate())
            {
                $user = new User();
                $user->user_name = $this->username;
                $user->user_email = $this->email;
                $user->user_status = 1;
                $user->user_verification_code = md5(uniqid(microtime(), true));
                $user->user_verification_date = date('Y-m-d H:i:s');
                $user->setPassword($this->password);
                $user->generateAuthKey();

                if ($user->save())
                {
                    return $user;
                }
            }
            else
            {
                return false;
            }
	}
}
