<?php

namespace app\models;

use yii\base\Model;
use yii\db\Query;
use app\models\Paste;

class Search extends Model
{
	public $query;
        
        public function rules()
        {
            return [
                 /*... */
                 [['query'], 'safe'],
            ];
        }

        public function search($params)
        {
            $query = new Query;
            $query->select('*')
            ->from('paste')
            ->where('content like "'.$params['query'].'"')
            ->orWhere('title like "'.$params['query'].'"')
            ->limit(10);
            return $query->all();
        }
}

?>