<?php

namespace app\models;

use yii\db\ActiveRecord;

class Setting extends ActiveRecord
{    
    public static function getValue($id)
    {
        if ($model = static::findOne(['setting_id' => $id ]) ){
            return $model->setting_value;
        }
        return false;
    }
    
    public static function setValue($id, $value, $user_id)
    {
        if ($model = static::findOne(['setting_id' => $id ]) ){
            $model->setting_value = $value;
            $model->setting_last_change_user = $user_id;
            return $model->save();
        }
        return false;
    }
}
