<?php

namespace app\models;

use Yii;
use yii\base\NotSupportedException;
use yii\db\ActiveRecord;
use yii\helpers\Security;
use yii\helpers\Url;    
use yii\web\IdentityInterface;

class User extends ActiveRecord implements IdentityInterface
{
	const UserStatusCreated = 1;
	const UserStatusConfirmed = 2;

	private $privileges;
	
	public function isAdmin()
	{
		return $this->user_administrator_type_id > 0;
	}
	
	public function hasPrivilege($name)
	{
		if ($this->user_administrator_type_id > 0)
		{
			if (is_null($this->privileges))
			{
				$this->privileges = Yii::$app->db->createCommand('SELECT * FROM user_group_permission WHERE user_group_permission_type_id = :typ')->bindValue(':typ', $this->user_administrator_type_id)->queryAll();
			}
			
			if ($this->privileges)
			{
				foreach ($this->privileges as $privilege)
				{
					if (strpos($privilege['user_group_permission_code'], $name) === 0)
					{
						return true;
					}
				}
			}
			
			if ($this->user_administrator_type_id == 1)
			{
				return true;
			}
		}
		
		return false;
	}
        
        public static function getAllIds()
        {
            $user_ids = Yii::$app->db->createCommand('SELECT user_id FROM user')->queryAll();
            $ids = [];
            foreach ($user_ids as $id)
            {
                $ids[] = $id['user_id'];
            }
            return $ids;
        }
        
        public function getGroup()
        {
            return $this->user_id!=0?$this->hasOne(UserGroup::className(), ['user_group_id' => 'user_administrator_type_id']):false;
        }
	
	public function getDisplayName()
	{
        return strlen($this->user_first_name) ? $this->user_first_name.' '. $this->user_last_name : trim( $this->user_name );
		return strlen($this->user_name) ? $this->user_name : trim($this->user_first_name . ' ' . $this->user_last_name);
	}

	public function getDisplaySex()
	{
		switch ($this->user_sex) {
			case 'K':
				$result = 'Famale';
				break;
			case 'M':
				$result = 'Male';
				break;
			default:
				$result = 'N/A';
				break;
		}
		return $result;
	}
	
	public static function findIdentity($id)
	{
		return static::findOne(['user_id' => $id, 'user_removed' => 0]);
	}
	
	public static function findIdentityByAccessToken($token, $type = null)
	{
		throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
	}

	public static function findByUsername($username)
	{
		return static::findOne(['user_name' => $username, 'user_removed' => 0, 'user_status' => 1]);
	}

	public static function findByFacebookId($id)
	{
		return static::findOne([
			'user_facebook_id' => $id,
			'user_status' => 1,
			'user_removed' => 0,
		]);
	}
        
        public static function findByGoogleId($id)
	{
		return static::findOne([
			'user_googleplus_id' => $id,
			'user_status' => 1,
			'user_removed' => 0,
		]);
	}
        
	public static function findByTwitterId($id)
	{
		return static::findOne([
			'user_twitter_id' => $id,
			'user_status' => 1,
			'user_removed' => 0,
		]);
	}
        
	public static function findByPasswordResetToken($token)
	{
		if (!static::isPasswordResetTokenValid($token))
		{
			return null;
		}

		return static::findOne([
			'uzytkownik_verification_code' => $token,
			'user_removed' => 0,
		]);
	}
        
        public function countUserPastes()
	{
            $result = Yii::$app->db->createCommand('SELECT count(*) AS count FROM paste WHERE user_id = :user_id')->bindValue(':user_id', $this->getId())->queryOne();
            return isset($result['count'])?$result['count']:0;
	}
        
        public function countUserFollowing()
	{
            $result = Yii::$app->db->createCommand('SELECT count(*) AS count FROM follow WHERE follow_followed_user_id = :follow_followed_user_id')->bindValue(':follow_followed_user_id', $this->getId())->queryOne();
            return isset($result['count'])?$result['count']:0;
	}
        
        public function countUserFollowed()
	{
            $result = Yii::$app->db->createCommand('SELECT count(*) AS count FROM follow WHERE follow_user_id = :follow_user_id')->bindValue(':follow_user_id', $this->getId())->queryOne();
            return isset($result['count'])?$result['count']:0;
	}
                
	public function getId()
	{
            return $this->getPrimaryKey();
	}
        
        public function getName()
	{
            return $this->user_name;
	}
        
	public function getAuthKey()
	{
            return $this->user_authorisation_code;
	}
        
        public function getUserPhotoUrl()
        {
            if ($this->user_photo)
            {
                return $this->user_photo;
            }
            $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
            $userDefaultAvatarFilename = Setting::getValue('USER_DEFAULT_AVATAR_FILENAME');
            return $directoryAsset.'/img/'.$userDefaultAvatarFilename;
        }
        
        public function getUserProfileUrl()
        {
            return Url::toRoute('/user/' . $this->user_name );
        }
        
        public function isFollowingUser($user_id)
        {
            return Follow::findOne(['follow_user_id' => $this->getId(), 'follow_followed_user_id' => $user_id ]);
        }

	public function validateAuthKey($authKey)
	{
            return $this->getAuthKey() === $authKey;
	}

	public function validatePassword($password)
	{
            if (isset($_SESSION['facebook_user_id']))
            {
                    if (self::findByFacebookId($_SESSION['facebook_user_id']))
                    {
                            return true;
                    }
            }

            return Yii::$app->security->validatePassword($password, $this->user_password);
	}

	public function setPassword($password)
	{
            $this->user_password = Yii::$app->security->generatePasswordHash($password);
	}

	public function generateAuthKey()
	{
            $this->user_authorisation_code = Yii::$app->security->generateRandomString();
	}

	public function generatePasswordResetToken()
	{
            $this->user_authorisation_code = Yii::$app->security->generateRandomString() . '_' . time();
	}

	public function removePasswordResetToken()
	{
            $this->user_authorisation_code = null;
	}
        
        public static function countAllUsers()
        {
            return (int)$countUsers = User::find()
            //->where('approved = 1')
            //->groupBy(['promoter_location_id', 'lead_type_id'])
            ->count();
        }  
}