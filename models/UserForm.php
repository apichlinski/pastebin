<?php

namespace app\models;

use Yii;
use yii\base\Model;

class UserForm extends Model
{
	public $username;
	public $password;
	public $password_repeat;
	public $userType;
	public $sex;
	public $lastname;
	public $firstname;
	public $email;
    public $location;
    public $education;
    public $skills;
    public $about;

	public function rules()
	{
		return [
			['username', 'required', 'message' => 'Podaj nazwę użytkownika'],
			['username', 'validateUsername'],
			['password', 'required', 'message' => 'Podaj hasło'],
                        //['about' => 'safe'],
			#['firstname', 'required', 'message' => 'Podaj imię'],
			#['lastname', 'required', 'message' => 'Podaj nazwisko'],
			#['sex', 'required', 'message' => 'Podaj płeć'],
			//['password', 'validatePassword'],
			['password', 'compare', 'message' => 'Podane hasła są różne'],
			['password_repeat', 'required', 'message' => 'Powtórz hasło'],
			['email', 'required'],
			['email', 'email', 'message' => 'Podaj porawny adres e-mail'],
		];
	}

	public function attributeLabels()
	{
		return [
			'username' => 'Username',
			'password' => 'Password',
			'password_repeat' => 'Repeat password',
			'userType' => 'Account type',
			'sex' => 'Sex',
			'firstname' => 'First name',
			'lastname' => 'Last name',
			'email' => 'E-mail address',
		];
	}

	public function validateUsername($attribute, $params)
	{
            if (!$this->hasErrors())
            {
                $db = Yii::$app->db;

                if ($user = $db->createCommand('SELECT * FROM user WHERE user_name = :user_name')->bindValue(':user_name', $this->username)->queryOne())
                {
                    $this->addError($attribute, 'Użytkownik o podanej nazwie jest już zarejestrowany');
                }
            }
	}
	
	public function validatePassword($attribute, $params)
	{
            return;

            if (!$this->hasErrors())
            {
                $user = $this->getUser();

                try
                {
                    if (!$user || !$user->validatePassword($this->password)) {
                        $this->addError($attribute, 'Niewłaściwa nazwa użytkownika lub hasło.');
                    }
                }
                catch (\Exception $e)
                {
                    $this->addError($attribute, 'Niewłaściwa nazwa użytkownika lub hasło.');
                }
            }
	}

	public function register()
	{
            if ($this->validate())
            {
                $user = new User();
                $user->user_name = $this->username;
                $user->user_email = $this->email;
                $user->user_status = 1;
                $user->user_verification_code = md5(uniqid(microtime(), true));
                $user->user_verification_date = date('Y-m-d H:i:s');
                $user->setPassword($this->password);
                $user->generateAuthKey();

                if ($user->save())
                {
                    return $user;
                }
            }
            else
            {
                return false;
            }
	}
        
        public function save()
	{
        print_r($user);die();
            $user = new User();
            if ($user->save())
            {
                return true;
            }
            else
            {
                return false;
            }
	}
}
