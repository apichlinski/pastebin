<?php

namespace app\models;

use yii\db\ActiveRecord;

class UserGroup extends ActiveRecord
{    
    public function getId()
    {
        return $this->getPrimaryKey();
    }
    
    public static function findIdentity($id)
    {
        return static::findOne(['user_group_id' => $id]);
    }
    
    public function deleteAllPermissions()
    {
        return UserGroupPermission::deleteAll('user_group_permission_type_id = '.$this->user_group_id);
    }
    
    public function addPermissions($permission_name = null)
    {
        if ($permission_name!=null)
        {
            $permission = new UserGroupPermission();            
            $permission->user_group_permission_type_id = $this->getId();
            $permission->user_group_permission_code = $permission_name;           	
            $permission->user_group_permission_name = $this->user_group_name.' '.$permission_name;

            return $permission->save();
        }
    }
}