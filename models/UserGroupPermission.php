<?php

namespace app\models;

use yii\db\ActiveRecord;

class UserGroupPermission extends ActiveRecord
{   
    public static function tableName()
    {
        return 'user_group_permission';
    }
    
    public function getId()
    {
        return $this->getPrimaryKey();
    }
}