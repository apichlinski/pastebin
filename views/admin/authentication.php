<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\PasteForm */

use yii\helpers\Html;
use yii\helpers\Url;   
use yii\bootstrap\ActiveForm;
use dosamigos\selectize\SelectizeDropDownList;

$this->title = 'Authentication';
$this->params['breadcrumbs'][] = ['label'=>'Admin','url' => [Url::toRoute('/admin')]];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::home() . 'plugins/select2/select2.min.css');
        
$this->registerJsFile(Url::home() . 'plugins/select2/select2.full.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJs('
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });');
?>

<?php if (Yii::$app->session->hasFlash('admin_authentication')): ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4>	<i class="icon fa fa-check"></i> Success!</h4>
        <?=Yii::$app->session->getFlash('admin_authentication')?>
    </div>
<?php endif; ?>

<div class="box box-success">
    <div class="box-header">
      <h3 class="box-title">Settings</h3>
    </div><!-- /.box-header -->
        <?php $form = ActiveForm::begin(['id' => 'pasts-form']); ?>
            <div class="box-body">
                <?= $form->field($model, 'login_required', ['options' => ['class' => 'checkbox icheck']] )->checkbox([
                    //'template' => "{input} {label}",
                ]) ?>
                
                 <?= $form->field($model, 'email_confirmation_required', ['options' => ['class' => 'checkbox icheck']] )->checkbox([
                    //'template' => "{input} {label}",
                ]) ?>
            </div>
        <div class="box-footer">    
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        </div>  
    <?php ActiveForm::end(); ?>
</div>


</div>

<div class="col-md-6">
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Facebook API</h3>
      </div><!-- /.box-header -->
      <!-- form start -->
        <?php $form = ActiveForm::begin(['id' => 'auth-facebook-form']); ?>
            <div class="box-body">
                <div class="form-group">
                    <p>Visit <a href="https://developers.facebook.com/" title="documentation">Facebook API documentation</a> for more examples and information about this plugin.</p>
                </div>
                <?= $form->field($facebook, 'active', ['options' => ['class' => 'checkbox icheck']] )->checkbox([
                  //'template' => "{input} {label}",
                ]) ?>
                <?= $form->field($facebook, 'app_id',[
                    'template' => "{label}\n{input}\n{hint}\n{error}",
                    'labelOptions' => ['class' => 'control-label'],
                ])->textInput([
                    'class'=>'form-control',
                    'type'=>'text',                    
                    'placeholder'=>'Enter App ID',    
                ]) ?>
                
                <?= $form->field($facebook, 'app_secret',[
                    'template' => "{label}\n{input}\n{hint}\n{error}",
                    'labelOptions' => ['class' => 'control-label'],
                ])->textInput([
                    'class'=>'form-control',
                    'type'=>'text',                    
                    'placeholder'=>'Enter App Secret',    
                ]) ?>
            </div><!-- /.box-body -->

            <div class="box-footer">
              <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'facebook-save-button']) ?>
            </div>
        <?php ActiveForm::end(); ?>
      </form>
    </div><!-- /.box -->
    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Twitter API</h3>
      </div>
      <!-- form start -->
      <?php $form = ActiveForm::begin(['id' => 'auth-twitter-form']); ?>
        <div class="box-body">
          <div class="form-group">
              <p>Visit <a href="https://dev.twitter.com/overview/documentation" title="documentation">Twitter API documentation</a> for more examples and information about this plugin.</p>
          </div>
          <?= $form->field($twitter, 'active', ['options' => ['class' => 'checkbox icheck']] )->checkbox([
            //'template' => "{input} {label}",
          ]) ?>
          <?= $form->field($twitter, 'api_key',[
              'template' => "{label}\n{input}\n{hint}\n{error}",
              'labelOptions' => ['class' => 'control-label'],
          ])->textInput([
              'class'=>'form-control',
              'type'=>'text',                    
              'placeholder'=>'Enter API Key',    
          ]) ?>
          <?= $form->field($twitter, 'api_secret',[
              'template' => "{label}\n{input}\n{hint}\n{error}",
              'labelOptions' => ['class' => 'control-label'],
          ])->textInput([
              'class'=>'form-control',
              'type'=>'text',                    
              'placeholder'=>'Enter API Secret',    
          ]) ?>
        </div><!-- /.box-body -->

        <div class="box-footer">
          <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'twitter-save-button']) ?>
        </div>
      <?php ActiveForm::end(); ?>
    </div><!-- /.box -->
</div>
<div class="col-md-6">
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Google Plus API</h3>
      </div>
      <!-- form start -->
      <?php $form = ActiveForm::begin(['id' => 'auth-google-form']); ?>
        <div class="box-body">
          <div class="form-group">
              <p>Visit <a href="https://developers.google.com/api-client-library/php/guide/aaa_overview" title="documentation">Google Plus API documentation</a> for more examples and information about this plugin.</p>
          </div>
          <?= $form->field($google, 'active', ['options' => ['class' => 'checkbox icheck']] )->checkbox([
            //'template' => "{input} {label}",
          ]) ?>
          <?= $form->field($google, 'client_id',[
              'template' => "{label}\n{input}\n{hint}\n{error}",
              'labelOptions' => ['class' => 'control-label'],
          ])->textInput([
              'class'=>'form-control',
              'type'=>'text',                    
              'placeholder'=>'Enter Client ID',    
          ]) ?>
          <?= $form->field($google, 'client_secret',[
              'template' => "{label}\n{input}\n{hint}\n{error}",
              'labelOptions' => ['class' => 'control-label'],
          ])->textInput([
              'class'=>'form-control',
              'type'=>'text',                    
              'placeholder'=>'Enter Client Secret',    
          ]) ?>
        </div><!-- /.box-body -->

        <div class="box-footer">
          <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'google-save-button']) ?>
        </div>
      <?php ActiveForm::end(); ?>
    </div><!-- /.box -->
