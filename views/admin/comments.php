<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;   

$this->title = 'Manage comments';
$this->params['breadcrumbs'][] = ['label'=>'Admin','url' => [Url::toRoute('/admin')]];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::home() . 'plugins/datatables/dataTables.bootstrap.css');
        
$this->registerJsFile(Url::home() . 'plugins/datatables/jquery.dataTables.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJsFile(Url::home() . 'plugins/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJs("
    $(function () {
      $('#example1').DataTable({
        'order': [[ 2, 'desc' ]],
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false
      });
    });");


?>

<div class="box box-success">
    <div class="box">
        <div class="box-header">
          <h3 class="box-title">All Comments</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Paste</th>
                <th>Username</th>
                <th>Created</th>
                <th>Updated</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php
                if (count($commentsList)>0) {
                    foreach ($commentsList as $comment) {
                      $user = $comment->author;
                      $userName = $user->user_name;
                      $userUrl = Url::toRoute('/admin/user-edit/' . $user->getId() );

                      $paste = $comment->paste;
                      $pasteName = $paste->title;
                      $pasteUrl = $comment->getPasteUrl().'#comment_'.$comment->getId();

                      $formatter = \Yii::$app->formatter;
                      $commentCreated = $formatter->asDate($comment->comment_creation_date,'medium');
                      $commentUpdated = $formatter->asDate($comment->comment_update_date,'medium');
                      ?>
                      <tr>
                          <td class="col-md-5"><a href="<?= $pasteUrl ?>" title="<?= $pasteName ?>"><?= $pasteName ?></a></td>
                          <td class="col-md-2"><a href="<?= $userUrl ?>" title="<?= $userName ?>"><?=$userName?></a></td>
                          <td class="col-md-2"><?= $commentCreated ?></td>
                          <td class="col-md-1"><?= $commentUpdated ?></td>
                          <td class="col-md-1"><a href="<?= Url::toRoute('/admin/user-remove/' . $user->getId() ) ?>" class="fa fa-trash-o"></a></td>

                      </tr>
                      <?php
                    }  
                }?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Paste</th>
                    <th>Username</th>
                    <th>Created</th>
                    <th>Updated</th>
                    <th></th>
                </tr>
            </tfoot>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
</div>