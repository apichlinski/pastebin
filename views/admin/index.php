<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\L;

$this->title = 'Dashboard';
$this->params['breadcrumbs'][] = ['label'=>'Admin','url' => [Url::toRoute('/admin')]];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css');
$this->registerCssFile(Url::home() . 'plugins/morris/morris.css');
$this->registerCssFile(Url::home() . 'plugins/jvectormap/jquery-jvectormap-1.2.2.css');


$this->registerJsFile('https://code.jquery.com/ui/1.11.4/jquery-ui.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
//$this->registerJsFile(Url::home() . 'plugins/morris/morris.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);

$this->registerJsFile(Url::home() . 'plugins/sparkline/jquery.sparkline.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);

$this->registerJsFile(Url::home() . 'plugins/jvectormap/jquery-jvectormap-1.2.2.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJsFile(Url::home() . 'plugins/jvectormap/jquery-jvectormap-world-mill-en.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);

$this->registerJsFile(Url::home() . 'plugins/knob/jquery.knob.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);

$this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);

$this->registerJsFile(Url::home() . 'plugins/fastclick/fastclick.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);

$this->registerJsFile(Url::home() . 'js/dashboard.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);

$this->registerJsFile(Url::home() . 'js/demo.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
?>

<!-- Small boxes (Stat box) -->
<div class="row">
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-aqua">
      <div class="inner">
        <h3><?=$countPastes?></h3>
        <p>New Pastes</p>
      </div>
      <div class="icon">
        <i class="ion ion-clipboard"></i>
      </div>
      <a href="<?=Url::toRoute('/admin/pastes/')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div><!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-green">
      <div class="inner">
        <h3><?=$countComments?></h3>
        <p>New comments</p>
      </div>
      <div class="icon">
        <i class="ion ion-android-chat"></i>
      </div>
      <a href="<?=Url::toRoute('/admin/comments/')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div><!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-yellow">
      <div class="inner">
        <h3><?=$countUsers?></h3>
        <p>User Registrations</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <a href="<?=Url::toRoute('/admin/users/')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div><!-- ./col -->
  <div class="col-lg-3 col-xs-6">
    <!-- small box -->
    <div class="small-box bg-red">
      <div class="inner">
        <h3>65</h3>
        <p>Unique Visitors</p>
      </div>
      <div class="icon">
        <i class="ion ion-pie-graph"></i>
      </div>
      <a href="<?=Url::toRoute('/admin/stats/')?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div><!-- ./col -->
</div><!-- /.row -->




<!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <section class="col-lg-7 connectedSortable">
        <!-- Custom tabs (Charts with tabs)-->
        <div class="nav-tabs-custom">
          <!-- Tabs within a box -->
          <ul class="nav nav-tabs pull-right">
            <li class="active"><a href="#revenue-chart" data-toggle="tab">Area</a></li>
            <li><a href="#sales-chart" data-toggle="tab">Donut</a></li>
            <li class="pull-left header"><i class="fa fa-inbox"></i> Sales</li>
          </ul>
          <div class="tab-content no-padding">
            <!-- Morris chart - Sales -->
            <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
            <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;"></div>
          </div>
        </div><!-- /.nav-tabs-custom -->
        
        <!-- USERS LIST -->
        <div class="box box-danger">
          <div class="box-header with-border">
            <h3 class="box-title">Latest Members</h3>
            <div class="box-tools pull-right">
              <span class="label label-danger">8 New Members</span>
              <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              <button class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body no-padding">
            <ul class="users-list clearfix">
                <?php foreach ($newUsersList as $newUser) { ?>
                    <li>
                      <img src="<?=$newUser->getUserPhotoUrl()?>" width="160" alt="User Image">
                      <a class="users-list-name" href="#"><?= $newUser->getDisplayName() ?></a>
                      <span class="users-list-date"><?= L::timeAgo($newUser->user_create_date); ?></span>
                    </li>
                <?php } ?>
            </ul><!-- /.users-list -->
          </div><!-- /.box-body -->
          <div class="box-footer text-center">
            <a href="<?= Url::toRoute('/admin/users') ?>" class="uppercase">View All Users</a>
          </div><!-- /.box-footer -->
        </div><!--/.box -->
      </section><!-- /.Left col -->
      <!-- right col (We are only adding the ID to make the widgets sortable)-->
      <section class="col-lg-5 connectedSortable">

        <!-- Map box -->
        <div class="box box-solid bg-light-blue-gradient">
          <div class="box-header">
            <!-- tools box -->
            <div class="pull-right box-tools">
              <button class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip" title="Date range"><i class="fa fa-calendar"></i></button>
              <button class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;"><i class="fa fa-minus"></i></button>
            </div><!-- /. tools -->

            <i class="fa fa-map-marker"></i>
            <h3 class="box-title">
              Visitors
            </h3>
          </div>
          <div class="box-body">
            <div id="world-map" style="height: 250px; width: 100%;"></div>
          </div><!-- /.box-body-->
          <div class="box-footer no-border">
            <div class="row">
              <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                <div id="sparkline-1"></div>
                <div class="knob-label">Visitors</div>
              </div><!-- ./col -->
              <div class="col-xs-4 text-center" style="border-right: 1px solid #f4f4f4">
                <div id="sparkline-2"></div>
                <div class="knob-label">Online</div>
              </div><!-- ./col -->
              <div class="col-xs-4 text-center">
                <div id="sparkline-3"></div>
                <div class="knob-label">Exists</div>
              </div><!-- ./col -->
            </div><!-- /.row -->
          </div>
        </div>
        <!-- /.box -->
      </section><!-- right col -->
    </div><!-- /.row (main row) -->