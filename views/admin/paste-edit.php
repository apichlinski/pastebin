<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\PasteForm */

use yii\helpers\Html;
use yii\helpers\Url;   
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use dosamigos\selectize\SelectizeDropDownList;

$this->title = 'Edit paste '.$paste->title;
$this->params['breadcrumbs'][] = ['label'=>'Admin','url' => [Url::toRoute('/admin')]];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::home() . 'plugins/select2/select2.min.css');
        
$this->registerJsFile(Url::home() . 'plugins/select2/select2.full.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJs('
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });');
?>
<div class="box box-success">
    <div class="box-header">
      <h3 class="box-title">Edit paste</h3>
    </div><!-- /.box-header -->
        <?php $form = ActiveForm::begin(['id' => 'pasts-form']); ?>
            
            <div class="box-body">
                <?= $form->field($model, 'content', ['inputOptions' => ['autofocus' => 'autofocus'] ])->textArea(['rows' => 6]) ?>

                <?= $form->field($model, 'title') ?>

                <?= $form->field($model, 'highlighting_id')->dropDownList($highlighting, ['class'=>'form-control select2','style'=>'width:100%;']); ?>

                <?= $form->field($model, 'tags')->dropDownList($tags, ['class'=>'form-control select2','multiple'=>'multiple','style'=>'width:100%;']); ?>
                
                <?= $form->field($model, 'user_id')->dropDownList($users, ['class'=>'form-control select2','style'=>'width:100%;']); ?>
                
                <?= $form->field($model, 'allowed_users')->dropDownList($users, ['class'=>'form-control select2','multiple'=>'multiple','style'=>'width:100%;']); ?>

                <?= $form->field($model, 'status_id')->dropDownList($status, ['class'=>'form-control select2','style'=>'width:100%;']); ?>

                <?= $form->field($model, 'active_from')->widget(DatePicker::classname(), [
                        'language' => 'pl',
                        'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'autoclose' => true
                        ]
                ]) ?>

                <?= $form->field($model, 'active_to')->widget(DatePicker::classname(), [
                        'language' => 'pl',
                        'pluginOptions' => [
                                'format' => 'yyyy-mm-dd',
                                'autoclose' => true
                        ]
                ]) ?>
            </div>
        <div class="box-footer">    
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
            <?= Html::submitButton('Save & stay', ['class' => 'btn btn-primary', 'name' => 'save-and-stay-button']) ?>
            <?= Html::submitButton('Cancel', ['class' => 'btn btn-default pull-right', 'name' => 'cancel-button']) ?>
        </div>  
    <?php ActiveForm::end(); ?>
</div>