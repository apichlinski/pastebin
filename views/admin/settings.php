<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\PasteForm */

use yii\helpers\Html;
use yii\helpers\Url;   
use yii\bootstrap\ActiveForm;
use dosamigos\selectize\SelectizeDropDownList;

$this->title = 'Settings';
$this->params['breadcrumbs'][] = ['label'=>'Admin','url' => [Url::toRoute('/admin')]];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::home() . 'plugins/select2/select2.min.css');
        
$this->registerJsFile(Url::home() . 'plugins/select2/select2.full.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJs('
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });');
?>
<div class="box box-success">
    <div class="box-header">
      <h3 class="box-title">Settings</h3>
    </div><!-- /.box-header -->
        <?php $form = ActiveForm::begin(['id' => 'pasts-form']); ?>
            <div class="box-body">
                <?= $form->field($model, 'enabled_google_adwords', ['options' => ['class' => 'checkbox icheck']] )->checkbox([
                    //'template' => "{input} {label}",
                ]) ?>
            </div>
        <div class="box-footer">    
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        </div>  
    <?php ActiveForm::end(); ?>
</div>