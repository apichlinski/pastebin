<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\PasteForm */

use yii\helpers\Html;
use yii\helpers\Url;   
use yii\bootstrap\ActiveForm;
use kartik\date\DatePicker;
use dosamigos\selectize\SelectizeDropDownList;

$this->title = 'Edit user '.$user->user_name;
$this->params['breadcrumbs'][] = ['label'=>'Admin','url' => [Url::toRoute('/admin')]];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::home() . 'plugins/select2/select2.min.css');
        
$this->registerJsFile(Url::home() . 'plugins/select2/select2.full.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJs('
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });');
?>
<div class="box box-success">
    <div class="box-header">
      <h3 class="box-title">Edit user</h3>
    </div><!-- /.box-header -->
        <?php $form = ActiveForm::begin(['id' => 'user-form']); ?>
            
            <div class="box-body">
                <?= $form->field($model, 'status', ['options' => ['class' => 'checkbox icheck']] )->checkbox([
                    //'template' => "{input} {label}",
                ]) ?>
                
                <?= $form->field($model, 'username') ?>
                
                <?= $form->field($model, 'firstname') ?>
                
                <?= $form->field($model, 'lastname') ?>
                
                <?= $form->field($model, 'email') ?>

                <?= $form->field($model, 'password') ?>
                
                <?= $form->field($model, 'password_repeat') ?>
                
                <?= $form->field($model, 'sex')->dropDownList(
                    ['F' => 'Famale', 'M' => 'Male'], ['prompt' => ''],
                    [
                        'class'=>'form-control select2',
                        'style'=>'width:100%;',
                        'type'=>'text',                    
                        'placeholder'=>'Sex',    
                    ]) ?>
                
                <?= $form->field($model, 'about', ['inputOptions' => ['autofocus' => 'autofocus'] ])->textArea(['rows' => 6]) ?>

                <?= $form->field($model, 'userAdminType')->dropDownList($user_group, 
                        [
                            'class'=>'form-control select2',
                            'style'=>'width:100%;',
                            'placeholder'=>'User type', 
                        ]); ?>

            </div>
        <div class="box-footer">    
            <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
            <?= Html::submitButton('Save & stay', ['class' => 'btn btn-primary', 'name' => 'save-and-stay-button']) ?>
            <?= Html::submitButton('Cancel', ['class' => 'btn btn-default pull-right', 'name' => 'cancel-button']) ?>
        </div>  
    <?php ActiveForm::end(); ?>
</div>