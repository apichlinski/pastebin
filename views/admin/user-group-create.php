<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url; 
use yii\bootstrap\ActiveForm;

$this->title = 'Create user group';
$this->params['breadcrumbs'][] = ['label'=>'Admin','url' => [Url::toRoute('/admin')]];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="box box-success">
    <div class="box-header">
      <h3 class="box-title">Create user group</h3>
    </div><!-- /.box-header -->
    <?php $form = ActiveForm::begin([
            'id' => 'user-group-form',
            //'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'options' => [
                    //'tag' => true,
                    'class' => 'form-group has-feedback',
                ],
            ],
        ]); ?>
        <div class="box-body">
            
            <?php echo $form->field($formModel, 'name',[
                    //'template' => "{label}\n<i class='fa fa-user'></i>\n{input}\n{hint}\n{error}"
                   
                ])->textInput([
                'type'=>'text',
                'class'=>'form-control', 
                'autofocus' => true, 
                'placeholder'=>'Group name',                
                
            ]) ?>          
    </div>
    <div class="box-footer">    
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        <?= Html::submitButton('Cancel', ['class' => 'btn btn-default pull-right', 'name' => 'cancel-button']) ?>
    </div>  
    <?php ActiveForm::end(); ?>
</div>