<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url; 
use yii\bootstrap\ActiveForm;

$this->title = 'Edit user group '.$formModel->name;
$this->params['breadcrumbs'][] = ['label'=>'Admin','url' => [Url::toRoute('/admin')]];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::home() . 'plugins/iCheck/square/blue.css');
$this->registerJsFile(Url::home() . 'plugins/iCheck/icheck.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset'] );
$this->registerJs("
     $(function () {
            $('input').iCheck({
              checkboxClass: 'icheckbox_square-blue',
              radioClass: 'iradio_square-blue',
              increaseArea: '20%' // optional
            });
          });"); 
?>

<div class="box box-success">
    <div class="box-header">
      <h3 class="box-title">Edit user group <?= $formModel->name ?></h3>
    </div><!-- /.box-header -->
    <?php $form = ActiveForm::begin([
            'id' => 'user-group-form',
            //'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'options' => [
                    //'tag' => true,
                    'class' => 'form-group has-feedback',
                ],
            ],
        ]); ?>
        <div class="box-body">
            
            <?php echo $form->field($formModel, 'name',[
                    //'template' => "{label}\n<i class='fa fa-user'></i>\n{input}\n{hint}\n{error}"
                   
                ])->textInput([
                'type'=>'text',
                'class'=>'form-control', 
                'autofocus' => true, 
                'placeholder'=>'Group name',                
                
            ]) ?>    
            
            <div class="form-group">
                <label class="control-label" for="adminusergroupform-name">Permissions</label>
                <?php foreach ($permissions as $permission_name => $permission_value ) { ?>                  
                    <div class="checkbox">
                        <label>
                            <input class="checkbox icheck" name="permissions[<?=$permission_name?>]" type="checkbox" <?= $permission_value?'checked':''?>>
                            <?=$permission_name?>
                        </label>
                    </div>
                <?php } ?>
            </div>
    </div>
    <div class="box-footer">    
        <?= Html::submitButton('Save', ['class' => 'btn btn-primary', 'name' => 'save-button']) ?>
        <?= Html::submitButton('Cancel', ['class' => 'btn btn-default pull-right', 'name' => 'cancel-button']) ?>
    </div>  
    <?php ActiveForm::end(); ?>
</div>