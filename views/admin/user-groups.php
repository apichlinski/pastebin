<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;   

$this->title = 'Manage user groups';
$this->params['breadcrumbs'][] = ['label'=>'Admin','url' => [Url::toRoute('/admin')]];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::home() . 'plugins/datatables/dataTables.bootstrap.css');
        
$this->registerJsFile(Url::home() . 'plugins/datatables/jquery.dataTables.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJsFile(Url::home() . 'plugins/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJs("
    $(function () {
      $('#example1').DataTable({
        'order': [[ 0, 'asc' ]],
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false
      });
    });");


?>

<div class="box box-success">
    <div class="box">
        <div class="box-header">
          <h3 class="box-title">Results from Users</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Group name</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php
                if (count($groupList)>0) {
                    foreach ($groupList as $group) {
                        $groupName = $group->user_group_name;
                        $groupUrl = Url::toRoute('/admin/user-group-edit/' . $group->getId() );
                      ?>
                      <tr>
                          <td class="col-md-11"><a href="<?= $groupUrl ?>" title="<?= $groupName ?>"><?=$groupName?></a></td>
                          <td class="col-md-1"><a href="<?= Url::toRoute('/admin/user-group-remove/' . $group->getId() ) ?>" class="fa fa-trash-o"></a></td>
                      </tr>
                      <?php
                    }  
                }?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Group name</th>
                    <th></th>
                </tr>
            </tfoot>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
</div>