<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;   

$this->title = 'Manage users';
$this->params['breadcrumbs'][] = ['label'=>'Admin','url' => [Url::toRoute('/admin')]];
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::home() . 'plugins/datatables/dataTables.bootstrap.css');
        
$this->registerJsFile(Url::home() . 'plugins/datatables/jquery.dataTables.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJsFile(Url::home() . 'plugins/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJs("
    $(function () {
      $('#example1').DataTable({
        'order': [[ 4, 'desc' ]],
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false
      });
    });");


?>

<div class="box box-success">
    <div class="box">
        <div class="box-header">
          <h3 class="box-title">All Users</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <table id="example1" class="table table-bordered table-hover">
            <thead>
              <tr>
                <th>Username</th>
                <th>Type</th>
                <th>Last logged</th>
                <th>Joined</th>
                <th>Pastes</th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              <?php
                if (count($usersList)>0) {
                    foreach ($usersList as $user) {
                        $userName = $user->user_name;
                        $userUrl = Url::toRoute('/admin/user-edit/' . $user->getId() );

                        $formatter = \Yii::$app->formatter;
                        $userJoined = $formatter->asDate($user->user_create_date,'medium');
                        $userLastLogged = $user->user_last_login_date!='0000'?$formatter->asDate($user->user_last_login_date,'medium'):'';
                        $userPastes = $user->countUserPastes();

                        if ($user->group!=false) {
                            $userGroup = $user->group->user_group_name;
                        } else {
                            $userGroup = 'User';
                        }
                      ?>
                      <tr>
                          <td class="col-md-5"><a href="<?= $userUrl ?>" title="<?= $userName ?>"><?=$userName?></a></td>
                          <td class="col-md-1"><?= $userGroup ?></td>
                          <td class="col-md-2"><?= $userLastLogged ?></td>
                          <td class="col-md-2"><?= $userJoined ?></td>
                          <td class="col-md-1"><?= $userPastes ?></td>
                          <td class="col-md-1"><a href="<?= Url::toRoute('/admin/user-remove/' . $user->getId() ) ?>" class="fa fa-trash-o"></a></td>

                      </tr>
                      <?php
                    }  
                }?>
            </tbody>
            <tfoot>
                <tr>
                    <th>Username</th>
                    <th>Type</th>
                    <th>Last logged</th>    
                    <th>Joined</th>
                    <th>Pasted</th>
                    <th></th>
                </tr>
            </tfoot>
          </table>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
</div>