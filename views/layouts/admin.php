<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

if (class_exists('backend\assets\AppAsset')) {
    backend\assets\AppAsset::register($this);
} else {
    app\assets\AppAsset::register($this);
}

dmstr\web\AdminLteAsset::register($this);

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="XwBdyhls2u2O2_fAYOldVHy-Nd-lBfgPluAcEt2-Iao" />
    <?php
    if (isset($this->blocks['block_head']))
        echo $this->blocks['block_head'];
    ?>

    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) .' | '. Yii::$app->name ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NBLZGX"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NBLZGX');</script>
    <!-- End Google Tag Manager -->
<?php $this->beginBody() ?>
<div class="wrapper">

    <?= $this->render(
        'header_admin.php',
        ['directoryAsset' => $directoryAsset]
    ) ?> 
    <?= $this->render(
        'left.php',
        ['directoryAsset' => $directoryAsset]
    )    
    ?>
    <?= $this->render(
        'content.php',
        ['content' => $content, 'directoryAsset' => $directoryAsset]
    ) ?>

</div>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
