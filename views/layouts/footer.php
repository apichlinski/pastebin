<footer class="main-footer">
    <?php if (Yii::$app->controller->id!='admin') { ?>
    <div class="container">
    <?php } ?>
        <div class="pull-right hidden-xs">
            <b>Version</b> <?=Yii::$app->params['PBVersion'] ?>
        </div>
        <strong>Copyright &copy; 2014-2017 <a href="http://apichlinski.pl">Andrzej Pichliński</a>.</strong> All rights
        reserved.
    <?php if (Yii::$app->controller->id!='admin') { ?>
    </div>
    <?php } ?>
</footer>