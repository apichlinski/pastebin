<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\models\Search;
use app\models\Setting;

/* @var $this \yii\web\View */
/* @var $content string */

if (isset(Yii::$app->user->identity)) {
    if (Yii::$app->user->identity->group!=false) {
        $userGroup = Yii::$app->user->identity->group->user_group_name;
    } else {
        $userGroup = 'User';
    }
}

$userDefaultAvatarFilename = Setting::getValue('USER_DEFAULT_AVATAR_FILENAME');
  ?>
<header class="main-header">

    

    <nav class="navbar navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a href="<?=Yii::$app->homeUrl?>" class="navbar-brand"><b>Paste</b>BIN</a>
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                  <i class="fa fa-bars"></i>
                </button>
            </div>
            
            <div id="navbar-collapse" class="collapse navbar-collapse pull-left">
                <?= dmstr\widgets\Menu::widget(
                    [
                        'options' => ['class' => 'nav navbar-nav'],
                        'items' => [
                            ['label' => 'New paste', 'url' => ['/'], 'options' => ['class' => (Yii::$app->controller->id=='site' && Yii::$app->controller->action->id=='index')?'active':'']],
                            ['label' => 'Trends', 'url' => ['/site/trends'], 'options' => ['class' => (Yii::$app->controller->id=='site' && Yii::$app->controller->action->id=='trends')?'active':'']],
                            ['label' => 'About', 'url' => ['/site/about'], 'options' => ['class' => (Yii::$app->controller->id=='site' && Yii::$app->controller->action->id=='about')?'active':'']],
                            //['label' => 'Admin', 'url' => ['/admin'], 'visible' => isset(Yii::$app->user->identity)?Yii::$app->user->identity->isAdmin():0],
                        ],
                    ]
                ) ?>
                <?php $form = ActiveForm::begin(['action' => Url::toRoute('/search'), 'id' => 'searchForm', 'options' => ['role' => 'search', 'class' => 'navbar-form navbar-left']]); ?>        
                    <?php echo $form->field(new Search, 'query',[
                            //'template' => "{label}\n<i class='fa fa-user'></i>\n{input}\n{hint}\n{error}"
                            'template' => "{input}",
                        ])->textInput([
                        'type'=>'text',
                        'class'=>'form-control',
                        'id'=>'navbar-search-input',
                        'placeholder'=>'Search',                

                    ]) ?>
                <?php ActiveForm::end(); ?>
            </div>
            
            <div class="navbar-custom-menu">

                <ul class="nav navbar-nav">
                    <!-- User Account: style can be found in dropdown.less -->

                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?php 
                            if (Yii::$app->user->isGuest) {
                            ?>
                                <img src="<?= $directoryAsset ?>/img/<?=$userDefaultAvatarFilename?>" class="user-image" alt="User Image"/>
                                <span class="hidden-xs">Anonymous</span>
                            <?php } else { ?>
                                <img src="<?=Yii::$app->user->identity->getUserPhotoUrl()?>" class="user-image" alt="User Image"/>
                                <span class="hidden-xs"><?=Yii::$app->user->identity->getDisplayName()?></span>
                            <?php }?>

                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <?php 
                                if (Yii::$app->user->isGuest) {
                                ?>
                                <img src="<?= $directoryAsset ?>/img/<?=$userDefaultAvatarFilename?>" class="img-circle"
                                     alt="User Image"/>
                                <p>
                                    Anonymous
                                    <small>Guest Account</small>
                                </p>
                                <?php } else { ?>
                                <img src="<?=Yii::$app->user->identity->getUserPhotoUrl()?>" class="img-circle"
                                     alt="User Image"/>
                                <p>
                                    <?=Yii::$app->user->identity->getDisplayName().' - '.(Yii::$app->user->identity->isAdmin()?$userGroup:'Registered user') ?>
                                    <small>Member since 
                                        <?php
                                        $formatter = \Yii::$app->formatter;
                                        echo $formatter->asDate(Yii::$app->user->identity->user_create_date,'php:M. Y');
                                        ?></small>
                                </p>
                                <?php }?>
                            </li>
                            <?php 
                            if (!Yii::$app->user->isGuest) {
                            ?>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <?= Html::a(
                                            'Followers',
                                            [Yii::$app->user->identity->getUserProfileUrl(),
                                            'tab' =>'followers' ]
                                                
                                        ) ?>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <?= Html::a(
                                            'Pastes',
                                            [Yii::$app->user->identity->getUserProfileUrl(),
                                            'tab' =>'active' ]
                                                
                                        ) ?>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                       <?= Html::a(
                                            'Comments',
                                            [Yii::$app->user->identity->getUserProfileUrl(),
                                            'tab' =>'comments' ]
                                                
                                        ) ?>
                                    </div>
                                </li>
                            <?php }?>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <?php if (Yii::$app->user->isGuest) { ?>
                                    <div class="pull-left">
                                        <?= Html::a(
                                            'Sign up',
                                            ['/site/registration'],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?>
                                    </div>
                                    <div class="pull-right">
                                        <?= Html::a(
                                            'Sign in',
                                            ['/site/login'],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?>
                                    </div>
                                <?php } else { ?>
                                    <div class="pull-left">
                                        <?= Html::a(
                                            'Profile',
                                            ['/user/'.Yii::$app->user->identity->getName()],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?>
                                    </div>
                                    <div class="pull-right">
                                        <?= Html::a(
                                            'Sign out',
                                            ['/site/logout'],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?>
                                    </div>
                                <?php } ?>
                            </li>
                        </ul>
                    </li>
                    
                    <?php if (isset(Yii::$app->user->identity)) {
                        if (Yii::$app->user->identity->isAdmin() ) { ?>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li>
                                <a href="<?= Url::toRoute('/admin') ?>" data-toggle="control-panel"><i class="fa fa-gears"></i></a>
                            </li>
                    <?php } } ?>
                </ul>
            </div>
        </div>
    </nav>
</header>
