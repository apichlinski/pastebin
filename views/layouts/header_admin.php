<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\models\Search;
use app\models\Setting;

/* @var $this \yii\web\View */
/* @var $content string */
$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
$userDefaultAvatarFilename = Setting::getValue('USER_DEFAULT_AVATAR_FILENAME');

if (Yii::$app->user->identity->group!=false) {
    $userGroup = Yii::$app->user->identity->group->user_group_name;
} else {
    $userGroup = 'User';
}
?>

<header class="main-header">
        <!-- Logo -->
        <a href="<?= Url::toRoute('/admin') ?>" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>P</b>BIN</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Paste</b>BIN</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="<?= Url::toRoute('/admin/toggle') ?>" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <?php 
                            if (Yii::$app->user->isGuest) {
                            ?>
                                <img src="<?= $directoryAsset ?>/img/<?=$userDefaultAvatarFilename?>" class="user-image" alt="User Image"/>
                                <span class="hidden-xs">Anonymous</span>
                            <?php } else { ?>
                                <img src="<?=Yii::$app->user->identity->getUserPhotoUrl()?>" class="user-image" alt="User Image"/>
                                <span class="hidden-xs"><?=Yii::$app->user->identity->getDisplayName()?></span>
                            <?php }?>

                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <?php 
                                if (Yii::$app->user->isGuest) {
                                ?>
                                <img src="<?= $directoryAsset ?>/img/<?=$userDefaultAvatarFilename?>" class="img-circle"
                                     alt="User Image"/>
                                <p>
                                    Anonymous
                                    <small>Guest Account</small>
                                </p>
                                <?php } else { ?>
                                <img src="<?=Yii::$app->user->identity->getUserPhotoUrl()?>" class="img-circle"
                                     alt="User Image"/>
                                <p>
                                    <?=Yii::$app->user->identity->getDisplayName().' - '.(Yii::$app->user->identity->isAdmin()?$userGroup:'Registered user') ?>
                                    <small>Member since 
                                        <?php
                                        $formatter = \Yii::$app->formatter;
                                        echo $formatter->asDate(Yii::$app->user->identity->user_create_date,'php:M. Y');
                                        ?></small>
                                </p>
                                <?php }?>
                            </li>
                            <?php 
                            if (!Yii::$app->user->isGuest) {
                            ?>
                                <!-- Menu Body -->
                                <li class="user-body">
                                    <div class="col-xs-4 text-center">
                                        <?= Html::a(
                                            'Followers',
                                            [Yii::$app->user->identity->getUserProfileUrl(),
                                            'tab' =>'followers' ]
                                                
                                        ) ?>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                        <?= Html::a(
                                            'Pastes',
                                            [Yii::$app->user->identity->getUserProfileUrl(),
                                            'tab' =>'active' ]
                                                
                                        ) ?>
                                    </div>
                                    <div class="col-xs-4 text-center">
                                       <?= Html::a(
                                            'Comments',
                                            [Yii::$app->user->identity->getUserProfileUrl(),
                                            'tab' =>'comments' ]
                                                
                                        ) ?>
                                    </div>
                                </li>
                            <?php }?>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <?php if (Yii::$app->user->isGuest) { ?>
                                    <div class="pull-left">
                                        <?= Html::a(
                                            'Sign up',
                                            ['/site/registration'],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?>
                                    </div>
                                    <div class="pull-right">
                                        <?= Html::a(
                                            'Sign in',
                                            ['/site/login'],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?>
                                    </div>
                                <?php } else { ?>
                                    <div class="pull-left">
                                        <?= Html::a(
                                            'Profile',
                                            ['/user/'.Yii::$app->user->identity->getName()],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?>
                                    </div>
                                    <div class="pull-right">
                                        <?= Html::a(
                                            'Sign out',
                                            ['/site/logout'],
                                            ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                        ) ?>
                                    </div>
                                <?php } ?>
                            </li>
                        </ul>
                    </li>
                    
                     <?php if (isset(Yii::$app->user->identity)) {
                        if (Yii::$app->user->identity->isAdmin() ) { ?>
                            <!-- User Account: style can be found in dropdown.less -->
                            <li>
                                <a href="<?= Url::toRoute('/') ?>" data-toggle="control-panel"><i class="fa fa-home"></i></a>
                            </li>
                    <?php } } ?>
            </ul>
          </div>
        </nav>
      </header>