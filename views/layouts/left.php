<?php 
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\models\Search;
?>
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?=Yii::$app->user->identity->getUserPhotoUrl()?>" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?=Yii::$app->user->identity->getDisplayName()?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <?php $form = ActiveForm::begin(
                [
                    'action' => Url::toRoute('/search'), 
                    'id' => 'searchForm', 
                    'options' => ['role' => 'search', 'class' => 'sidebar-form'],
                    'fieldConfig' => ['options' => ['class' => 'input-group']],
                ]); ?>        
            <?php echo $form->field(new Search, 'query',[
                    //'template' => "{label}\n<i class='fa fa-user'></i>\n{input}\n{hint}\n{error}"
                    'template' => "{input}<span class='input-group-btn'><button type='submit' name='search' id='search-btn' class='btn btn-flat'><i class='fa fa-search'></i></button></span>",
                ])->textInput([
                'type'=>'text',
                'class'=>'form-control',
                'id'=>'navbar-search-input',
                'placeholder'=>'Search',                

            ]) ?>
        <?php ActiveForm::end(); ?>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Main navigation', 'options' => ['class' => 'header']],
                    ['label' => 'Homepage', 'icon' => 'fa fa-home', 'url' => ['/']],
                    ['label' => 'Dashboard', 'icon' => 'fa fa-dashboard', 'url' => ['/admin/index']],                    
                    [
                        'label' => 'Pastes', 
                        'icon' => 'fa fa-file-text-o', 
                        'url' => ['#'],
                        'items' => [
                            ['label' => 'Create paste', 'icon' => 'fa fa-edit', 'url' => ['/admin/paste-create'],],
                            ['label' => 'Manage pastes', 'icon' => 'fa fa-file-code-o', 'url' => ['/admin/pastes'],],
                        ],
                    ],
                    [
                        'label' => 'Users', 
                        'icon' => 'fa fa-users', 
                        'url' => ['#'],
                        'items' => [
                            ['label' => 'Create user', 'icon' => 'fa fa-user-plus', 'url' => ['/admin/user-create'],],
                            ['label' => 'Manage users', 'icon' => 'fa fa-users', 'url' => ['/admin/users'],],
                            ['label' => 'Create group', 'icon' => 'fa fa-user-plus', 'url' => ['/admin/user-group-create'],],
                            ['label' => 'Manage groups', 'icon' => 'fa fa-group', 'url' => ['/admin/user-groups'],],
                        ],
                    ],
                    [
                        'label' => 'Comments', 
                        'icon' => 'fa fa-comments-o', 
                        'url' => ['#'],
                        'items' => [
                            ['label' => 'Pending comments', 'icon' => 'fa fa-user-plus', 'url' => ['/admin/comments-pending'],],
                            ['label' => 'Manage comments', 'icon' => 'fa fa-comments-o', 'url' => ['/admin/comments'],],
                        ],
                    ],
                    ['label' => 'Lookup', 'icon' => 'fa fa-file-code-o', 'url' => ['/admin/lookup']],
                    [
                        'label' => 'Settings', 
                        'icon' => 'fa fa-gears', 
                        'url' => ['#'],
                        'items' => [
                            ['label' => 'Basic settings', 'icon' => 'fa fa-gear', 'url' => ['/admin/settings'],],
                            ['label' => 'Authorisation', 'icon' => 'fa fa-sign-in', 'url' => ['/admin/authentication'],],
                        ],                        
                    ],
                    ['label' => 'Stats', 'icon' => 'fa fa-bar-chart-o', 'url' => ['/admin/stats']],
                    
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']],
                    ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                ],
            ]
        ) ?>

    </section>

</aside>
