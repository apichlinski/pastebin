<?php

use yii\helpers\Html;
use yii\helpers\Url;
use backend\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

dmstr\web\AdminLteAsset::register($this);

$this->registerCssFile(Url::home() . 'plugins/iCheck/square/blue.css');
$this->registerJsFile(Url::home() . 'plugins/iCheck/icheck.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset'] );
$this->registerJs("
     $(function () {
            $('input').iCheck({
              checkboxClass: 'icheckbox_square-blue',
              radioClass: 'iradio_square-blue',
              increaseArea: '20%' // optional
            });
          });"); 
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="XwBdyhls2u2O2_fAYOldVHy-Nd-lBfgPluAcEt2-Iao" />
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) .' | '. Yii::$app->name ?></title>
    <?php $this->head() ?>
</head>
<body class="hold-transition <?= Yii::$app->controller->action->id=='login'?'login-page':'register-page'?>">
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NBLZGX"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-NBLZGX');</script>
    <!-- End Google Tag Manager -->
    <?php $this->beginBody() ?>

        <?= $content ?>

    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
