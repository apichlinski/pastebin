<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */



if (Yii::$app->controller->action->id === 'login' || Yii::$app->controller->action->id === 'registration' || Yii::$app->controller->action->id === 'forgotten') { 
/**
 * Do not use this code in your template. Remove it. 
 * Instead, use the code  $this->layout = '//main-login'; in your controller.
 */
    echo $this->render(
        'main-login',
        ['content' => $content]
    );
}
elseif (Yii::$app->controller->id === 'admin') {
    echo $this->render(
        'admin',
        ['content' => $content]
    );
} else {

    if (class_exists('backend\assets\AppAsset')) {
        backend\assets\AppAsset::register($this);
    } else {
        app\assets\AppAsset::register($this);
    }

    dmstr\web\AdminLteAsset::register($this);

    $directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');
    ?>
    <?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="canonical" href="https://www.pastebin.com.pl">
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <link rel="manifest" href="manifest.json" />
        <!-- Add to home screen for Safari on iOS -->
        <meta name="theme-color" content="#FFF">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
        <meta name="apple-mobile-web-app-title" content="PasteBIN">
        <link rel="apple-touch-icon" href="images/icons/icon-152x152.png">
        <meta name="msapplication-TileImage" content="images/icons/icon-144x144.png">
        <meta name="msapplication-TileColor" content="#FFF">
        <meta name="google-site-verification" content="XwBdyhls2u2O2_fAYOldVHy-Nd-lBfgPluAcEt2-Iao" />
        <?php
        if (isset($this->blocks['block_head']))
            echo $this->blocks['block_head'];
        ?>
        
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) .' | '. Yii::$app->name ?></title>
        <?php $this->head() ?>

    </head>
    <body class="hold-transition skin-blue sidebar-mini layout-top-nav">
        <!-- Google Tag Manager -->
        <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-NBLZGX"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-NBLZGX');</script>
        <!-- End Google Tag Manager -->
    <?php $this->beginBody() ?>
    <div class="wrapper">

        <?= $this->render(
            'header.php',
            ['directoryAsset' => $directoryAsset]
        ) ?>
        <?php /*
        <?= $this->render(
            'left.php',
            ['directoryAsset' => $directoryAsset]
        )
        ?>
        */ 
        ?>
        <?= $this->render(
            'content.php',
            ['content' => $content, 'directoryAsset' => $directoryAsset]
        ) ?>

    </div>

        <?php $this->endBody() ?>
        <script src="js/app-loader.js" async></script>
    </body>
    </html>
    <?php $this->endPage() ?>
<?php } ?>
