<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;    
use yii\bootstrap\ActiveForm;

$this->title = 'Search for \''.$query.'\'';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::home() . 'plugins/datatables/dataTables.bootstrap.css');
        
$this->registerJsFile(Url::home() . 'plugins/datatables/jquery.dataTables.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJsFile(Url::home() . 'plugins/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJs("
    $(function () {
      $('#example1').DataTable({
        'order': [[ 2, 'desc' ]],
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
      });
      $('#example2').DataTable({
        'order': [[ 3, 'desc' ]],
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
      });
    });");


?>
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Results from Users</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
      <table id="example1" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>Username</th>
            <th>Joined</th>
            <th>Pastes</th>
          </tr>
        </thead>
        <tbody>
          <?php
            if (count($usersList)>0) {
                foreach ($usersList as $user) {
                    $userName = $user->user_name;
                    $userUrl = Url::toRoute('/user/' . $user->user_name );
                    
                    $formatter = \Yii::$app->formatter;
                    $userJoined = $formatter->asDate($user->user_create_date,'medium');
                    $userPastes = $user->countUserPastes();
                   

                  ?>
                  <tr>
                      <td class="col-md-5"><a href="<?= $userUrl ?>" title="<?= $userName ?>"><?=$userName?></a></td>
                      <td class="col-md-2"><?= $userJoined ?></td>
                      <td class="col-md-1"><?= $userPastes ?></td>
                      
                  </tr>
                  <?php
                }  
            }?>
        </tbody>
        <tfoot>
            <tr>
                <th>Title</th>
                <th>Joined</th>
                <th>Pasted</th>
            </tr>
        </tfoot>
      </table>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
<div class="box">
    <div class="box-header">
      <h3 class="box-title">Results from Pastes</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
      <table id="example2" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>Title</th>
            <th>Type</th>
            <th>Added</th>
            <th>Views</th>
            <th>User</th>
          </tr>
        </thead>
        <tbody>
          <?php
            if (count($usersList)>0) {
                foreach ($pastesList as $model) {
                  $pasteTitle = Html::encode($model->title);
                  $pasteUrl = Url::toRoute('/paste/' . $model->getId() );
                  $pasteAdded = $model->insert_date;
                  $pasteViews = $model->views;
                  $pasteType = $model->highlighting->name;
                  $user = $model->author;
                  if ($user!==false) {
                      $userUrl = $user->getUserProfileUrl();
                      $userName = $user->user_name;

                  }
                  ?>
                  <tr>
                      <td class="col-md-5"><a href="<?= $pasteUrl ?>" title="<?= $pasteTitle ?>"><?= $pasteTitle ?></a></td>
                      <td class="col-md-1"><?= $pasteType ?></td>
                      <td class="col-md-2"><?= $pasteAdded ?></td>
                      <td class="col-md-1"><?= $pasteViews ?></td>
                      <td class="col-md-2">
                          <?php if ($user!==false) { ?>
                              <a href="<?= $userUrl ?>" title="<?= $userName ?>"><?=$userName?></a>
                          <?php } else { ?>
                              Anonymous
                          <?php } ?>
                      </td>
                  </tr>
                  <?php
                }
          } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>Title</th>
                <th>Type</th>
                <th>Added</th>
                <th>Views</th>
                <th>User</th>
            </tr>
        </tfoot>
      </table>
    </div><!-- /.box-body -->
  </div><!-- /.box -->