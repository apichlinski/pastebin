<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-danger">
    <div class="box-header">
      <h3 class="box-title">About PasteBIN project</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <p>
            Hello,
        </p>
        <p>
            Welcome to my  page. You may modify the following file to customize its content:
        </p>
        <p>
            Basic assumptions:
            <ul>
                <li>syntax highlighting (you can use for ex. GeSHi)</li>
                <li>storing data in the database </li>
                <li>immunity application on SQL Injection attacks but also on JavaScript (XSS and CSRF) </li>
                <li>authentication using standard login, OpenID, Mozilla Persona</li>
                <li>advanced access control, including restriction to create and reading</li>
                <li>the possibility of determining the validity dates for pases, which will be removed from the database</li>
                <li>easy configurability of the application</li>
                <li>attractive appearance </li>
            </ul>
        </p>
        <p>
            <strong>Requirements:</strong> Good knowledge of web technologies such as PHP/MySQL and security issues
        </p>        
        <p>
            <strong>Type of work:</strong> Engineering 
        </p>
    </div>
</div>