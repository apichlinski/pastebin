<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Forgotten';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-box">
      <div class="login-logo">
        <a href="<?=Yii::$app->homeUrl?>"><b>Paste</b>BIN</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Create new password</p>
        <?php $form = ActiveForm::begin([
            'id' => 'forgotten-form',
            //'options' => ['class' => 'form-horizontal'],
            'fieldConfig' => [
                'options' => [
                    //'tag' => true,
                    'class' => 'form-group has-feedback',
                ],
            ],
        ]); ?>
        
        
            <?php echo $form->field($model, 'email',[
                    //'template' => "{label}\n<i class='fa fa-user'></i>\n{input}\n{hint}\n{error}"
                    'template' => "{input}\n<span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>",
                ])->textInput([
                'type'=>'email',
                'class'=>'form-control', 
                'autofocus' => true, 
                'placeholder'=>'Email',                
                
            ]) ?>
            <div class="row">
                <div class="col-xs-4">                    
                    <?= Html::submitButton('Reset', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'forgotten-button']) ?>
                </div><!-- /.col -->
            </div>
        <?php ActiveForm::end(); ?>

        <a href="<?=Url::toRoute('/login')?>">Return to sign in</a><br>
        <a href="<?=Url::toRoute('/registration')?>" class="text-center">Register a new membership</a>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->
