<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\PasteForm */

use yii\helpers\Html;
use yii\helpers\Url;   
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use kartik\date\DatePicker;
use dosamigos\selectize\SelectizeDropDownList;

$this->title = 'Create new paste';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::home() . 'plugins/select2/select2.min.css');
        
$this->registerJsFile(Url::home() . 'plugins/select2/select2.full.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJs('
    $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
    });');
?>
<?php if (Yii::$app->session->hasFlash('forgotten_submitted')): ?>
    <div class="alert alert-success alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4>	<i class="icon fa fa-check"></i> Success!</h4>
        New password has been sent. Please check your email.
    </div>
<?php endif; ?>


<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">New paste</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        <?php if ($isAvailable) { ?>
        <?php $form = ActiveForm::begin(['id' => 'pasta-form']); ?>

            <?= $form->field($model, 'content', ['inputOptions' => ['autofocus' => 'autofocus'] ])->textArea(['rows' => 6]) ?>

            <?= $form->field($model, 'title') ?>

            <?= $form->field($model, 'highlighting_id')->dropDownList($highlighting, ['class'=>'form-control select2','style'=>'width:100%;']); ?>

            <?= $form->field($model, 'tags')->dropDownList($tags, ['class'=>'form-control select2','multiple'=>'multiple','style'=>'width:100%;']); ?>
        
            <?= $form->field($model, 'allowed_users')->dropDownList($users, ['class'=>'form-control select2','multiple'=>'multiple','style'=>'width:100%;']); ?>

            <?= $form->field($model, 'status_id')->dropDownList($status, ['class'=>'form-control select2','style'=>'width:100%;']); ?>

            <?= $form->field($model, 'active_from')->widget(DatePicker::classname(), [
                    'language' => 'pl',
                    'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'autoclose' => true
                    ]
            ]) ?>

            <?= $form->field($model, 'active_to')->widget(DatePicker::classname(), [
                    'language' => 'pl',
                    'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'autoclose' => true
                    ]
            ]) ?>

            <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
            ]) ?>

            <div class="form-group">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'pasta-button']) ?>
            </div>

        <?php ActiveForm::end(); ?>
        
        <?php } else { ?>
            <p>Please <a href="<?=Url::toRoute('/login/')?>" title="login">sing in</a> or <a href="<?=Url::toRoute('/registration/')?>" title="login">sign up</a> to leave a paste.</p>
        <?php } ?>
    </div>
</div>
