<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;  
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;
use app\models\User;
use app\models\Setting;
use app\components\L;
use yii\widgets\Pjax;

$title = Html::encode($model->title);
$intro = strip_tags(substr($model->content, 0, 200) );
$intro = trim(preg_replace('/\s\s+/', ' ', $intro));

if ($title == 'Untitled') {
    $this->params['breadcrumbs'][] = $title;
    $this->title = substr($model->content, 0, 80);
} else {
    $this->title = $title;
    $this->params['breadcrumbs'][] = $title;
}

$this->registerCssFile('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css');

$directoryAsset = Yii::$app->assetManager->getPublishedUrl('@vendor/almasaeed2010/adminlte/dist');

$userDefaultAvatarFilename = Setting::getValue('USER_DEFAULT_AVATAR_FILENAME');
?>

<?php $this->beginBlock('block_head', true) ?>
<meta name="description" content="<?= $intro ?>">
        
<meta property=”og:title” content=”<?= $this->title ?>”/>
<meta property=”og:description” content=”<?= $intro ?>”/>
<meta property=”og:type” content=”article”/>
<meta property=”og:image” content=”http://www.pastebin.com.pl/image/facebook.png”/>
<meta property=”og:url” content=”http://www.pastebin.com.pl<?= Url::toRoute('/paste/' .$model->getId() ) ?>”/>
<meta property=”fb:admins” content=”USER_ID”/>
<?php $this->endBlock() ?>

<?php if (Yii::$app->session->hasFlash('pasteFormSubmitted')): ?>
    <div class="alert alert-success">
        Thank you for creating Paste.
    </div>
<?php endif; ?>

<div class="box box-danger">
    <div class="box-header with-border">
        <h3 class="box-title">
            <span class="pull-left badge bg-blue"><?=strtoupper($model->highlighting->name)?> </span>&nbsp;
            <?=$this->title?>
        </h3>
    </div><!-- /.box-header -->
    <div class="box-body">
        
        <div class="post">
            <div class="user-block">
              <?php
                  if ($model->author) {  
                  ?>
                      <img class="img-circle img-bordered-sm" src="<?= $model->author->getUserPhotoUrl() ?>" alt="user image">
                          <span class="username">
                            <a href="<?= $model->author->getUserProfileUrl() ?>"><?= $model->author->getDisplayName() ?></a>
                            <!--<a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>-->
                        </span>
                  <?php
                  } else {
                      ?>                      
                      <img class="img-circle img-bordered-sm" src="<?= $directoryAsset ?>/img/<?=$userDefaultAvatarFilename?>" alt="user image">
                      <span class="username">
                          Guest
                      </span>
                      <?php
                  }
              ?>
              <span class="description">Shared publicly - <?= L::timeAgo($model->insert_date); ?></span>
            </div><!-- /.user-block -->
            <p>
                <?php
                    Yii::$app->sc->setStart(0);
                    Yii::$app->sc->collect($model->highlighting->name, $model->content);
                    Yii::$app->sc->renderSourceBox();
                ?>
            </p>
            <ul class="list-inline">
              <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Views (<?=$model->views;?>)</a></li>
              <!--<li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li>-->
              <!--<li><a href="#" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a></li>-->
              <li class="pull-right"><a href="#" class="link-black text-sm"><i class="fa fa-comments-o margin-r-5"></i> Comments (<?= count($comments); ?>)</a></li>
            </ul>
        </div>
    </div>
</div>
<?php if (Setting::getValue('PASTE_COMMENT_ACTIVE')) { ?>

<!-- comments -->
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h3 class="box-title">All comments</h3>
            </div>
            <?php Pjax::begin(['id'=>'comments_list']); ?>
            <div class="box-body commments-box" id="all_comments">              
                <?php if ($comments) { 
                    foreach ($comments as $comment) {
                    ?>                
                        <!-- post -->
                        <div class="post" id="<?='comment_'.$comment->getId();?>">
                            <div class="user-block">
                                <?php
                                if ($comment->author) {  
                                ?>
                                    <img class="img-circle img-bordered-sm" src="<?= $comment->author->getUserPhotoUrl() ?>" alt="user image">
                                        <span class="username">
                                          <a href="<?= $comment->author->getUserProfileUrl() ?>"><?= $comment->author->getDisplayName() ?></a>
                                          <!--<a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>-->
                                      </span>
                                <?php
                                } else {
                                    ?>
                                    <span class="username">
                                        Guests
                                    </span>
                                    <?php
                                }
                                ?>
                              
                              <span class="description">Commented publicly - <?= L::timeAgo($comment->comment_creation_date); ?></span>
                            </div><!-- /.user-block -->
                            <p><?= $comment->comment_content ?></p>
                            <ul class="list-inline">
                                <li>
                                    <a href="<?=$comment->getPasteUrl()?>#<?='comment_'.$comment->getId();?>" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a>
                                </li>
                                <li>
                                <?= Html::a('<i class="fa fa-thumbs-o-up margin-r-5"></i> Like', '#', [
                                    'data'=>[
                                        'method' => 'post',
                                        'params'=>['comment-like-button'=>'1', 'comment_id' => $comment->comment_id ],
                                    ],
                                    'class'=>'link-black text-sm likeclicked',
                                ]) ?>
                                <?= Html::a('<i class="fa fa-thumbs-o-down margin-r-5"></i> Unlike', '#', [
                                    'data'=>[
                                        'method' => 'post',
                                        'params'=>['comment-unlike-button'=>'1', 'comment_id' => $comment->comment_id ],
                                    ],
                                    'class'=>'link-black text-sm likeclicked',
                                ]) ?>
                                    (<?= $comment->countLikes() ?>)
                                </li>
                            </ul>     
                        </div>
                        <!-- /post -->
                    <?php } ?>                    
                <?php
                } else { ?>
                    <div class="row">
                        <div class="col-sm-12">
                            <p>There are no comments yet, but You can be first one to comment this paste.</p>
                        </div>
                    </div>
                        
                <?php } ?>
                <?php $form = ActiveForm::begin([
                        'id' => 'comment-form',
                        'options' => [
                            'class'=>'form-horizontal',
                        ],
                        'fieldConfig' => [
                            'options' => [
                                'tag' => false,
                            ],
                        ],
                    ]); ?>
                    <div class="form-group margin-bottom-none">
                        <div class="col-sm-9">
                            <?php echo $form->field($commentForm, 'content',[
                                    'template' => "{input}",
                                ])->textInput([
                                'type'=>'text',
                                'class'=>'form-control input-sm', 
                                'placeholder'=>'Type a comment',                
                            ]) ?>
                        </div>
                        <?= Html::hiddenInput('comment-button', '1'); ?>
                        <div class="col-sm-3">
                            <?= Html::submitButton('Send', ['class' => 'btn btn-danger pull-right btn-block btn-sm', 'name' => 'comment-button']) ?>
                        </div>
                    </div>
                <?php ActiveForm::end(); ?>                    
            </div><!-- /box-body comments-box -->
            <?php Pjax::end(); ?>
        </div><!-- /box box-solid -->
    </div><!-- /col-sm-12 -->
</div><!-- /row -->

<?php } 

$script = <<< JS
    
   $(document).ready(function () {
        $('body').on('beforeSubmit', 'form#comment-form', function () {
            var form = $(this);
            // return false if form still have some validation errors
            if (form.find('.has-error').length) 
            {
                return false;
            }
            // submit form
            $.ajax({
            url    : form.attr('action'),
            type   : form.attr('method'),
            data   : form.serialize(),
            success: function (response) 
            {
                //var getupdatedata = $(responise).find('#all_comments');
                $.pjax.reload({container:'#comments_list'}); //for pjax update
                //$('#all_comments').html(getupdatedata);
                //console.log(getupdatedata);
            },
            error  : function () 
            {
                console.log('internal server error');
            }           
            });
            return false;
         });
        
        $('body').on('click', '.likeclicked', function () {
            var form = $(this);
            // submit form
            $.ajax({
            url    : '#',
            type   : form.data('method'),
            data   : form.data('params'),
            success: function (response) 
            {
                $.pjax.reload({container:'#comments_list'}); //for pjax update
            },
            error  : function () 
            {
                console.log('internal server error');
            }           
            });
            return false;
         });
    });
    
JS;
       
$this->registerJs($script);
?>