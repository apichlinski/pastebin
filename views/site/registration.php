<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use app\models\Setting;

$this->title = 'Registration';
$this->params['breadcrumbs'][] = $this->title;

$facebook  = Setting::getValue('AUTH_FACEBOOK_API_ACTIVE');
$google    = Setting::getValue('AUTH_GOOGLE_API_ACTIVE');
$twitter   = Setting::getValue('AUTH_TWITTER_API_ACTIVE');

?>
<div class="register-box">
      <div class="register-logo">
        <a href="<?=Yii::$app->homeUrl?>"><b>Paste</b>BIN</a>
      </div>

      <div class="register-box-body">
        <?php if (Yii::$app->session->hasFlash('profile_created')) {  ?>
            <div class="text alert alert-success">
                <?= Yii::$app->session->getFlash('profile_created') ?>
            </div>
        <?php } ?>
        <p class="login-box-msg">Register a new membership</p>
            <?php $form = ActiveForm::begin([
                'id' => 'registration-form',
                //'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'options' => [
                        //'tag' => true,
                        'class' => 'form-group has-feedback',
                    ],
                ],
            ]); ?>
            <?php echo $form->field($registerModel, 'username',[
                    //'template' => "{label}\n<i class='fa fa-user'></i>\n{input}\n{hint}\n{error}"
                    'template' => "{input}\n<span class=\"glyphicon glyphicon-user form-control-feedback\"></span>",
                ])->textInput([
                'type'=>'text',
                'class'=>'form-control', 
                'autofocus' => true, 
                'placeholder'=>'Username',                
                
            ]) ?>            
            <?php echo $form->field($registerModel, 'email',[
                    //'template' => "{label}\n<i class='fa fa-user'></i>\n{input}\n{hint}\n{error}"
                    'template' => "{input}\n<span class=\"glyphicon glyphicon-envelope form-control-feedback\"></span>",
                ])->textInput([
                'type'=>'text',
                'class'=>'form-control', 
                'autofocus' => true, 
                'placeholder'=>'Email',                
                
            ]) ?>
            <?php echo $form->field($registerModel, 'password',[
                    //'template' => "{label}\n<i class='fa fa-user'></i>\n{input}\n{hint}\n{error}"
                    'template' => "{input}\n<span class=\"glyphicon glyphicon-lock form-control-feedback\"></span>",
                ])->passwordInput([
                'type'=>'password',
                'class'=>'form-control', 
                'placeholder'=>'Password',                
                
            ]) ?>
            <?php echo $form->field($registerModel, 'password_repeat',[
                    //'template' => "{label}\n<i class='fa fa-user'></i>\n{input}\n{hint}\n{error}"
                    'template' => "{input}\n<span class=\"glyphicon glyphicon-log-in form-control-feedback\"></span>",
                ])->passwordInput([
                'type'=>'password',
                'class'=>'form-control', 
                'placeholder'=>'Password',                
                
            ]) ?>
          <div class="row">
            <div class="col-xs-8">
                <?= $form->field($registerModel, 'agreement', ['options' => ['class' => 'checkbox icheck']] )->checkbox([
                       'template' => "{beginLabel}{input} I agree to the <a href='".Url::toRoute('/terms')."'>terms</a>{endLabel}",
                ]) ?>
            </div><!-- /.col -->
            <div class="col-xs-4">
              <?= Html::submitButton('Register', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'register-button']) ?>
            </div><!-- /.col -->
          </div>
        <?php ActiveForm::end(); ?>

        <div class="social-auth-links text-center">
            <?php 
                if ($facebook || $google) {
                    echo '<p>- OR -</p>';
                }
                $domain = Yii::$app->params['domainName'];
                if ($facebook)
                {
                    $fb = new Facebook\Facebook([
                           'app_id' => '960989454004531',
                            'app_secret' => '4d18ed2c60fec150cebff249c9919952',
                            'default_graph_version' => 'v2.3',
                    ]);

                    $helper = $fb->getRedirectLoginHelper();
                    $permissions = ['email', 'user_likes']; // optional
                    
                    $loginFacebookUrl = $helper->getLoginUrl($domain.'/fblogin', $permissions);


                    echo '<a href="'.$loginFacebookUrl.'" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>';
                }
                if ($google)
                {
                    $client = new Google_Client();
                    $client->setClientId(Yii::$app->params['GClientID']);
                    $client->setClientSecret(Yii::$app->params['GClientSecret']);
                    $client->setRedirectUri($domain.'/gglogin');
                    $client->setScopes(['email']);
                    //$client->setAccessType('offline');
                    
                    $loginGoogleUrl = $client->createAuthUrl();
                    echo '<a href="'.$loginGoogleUrl.'" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>';
                }
                if ($twitter)
                {
                    $consumer_key = Setting::getValue('AUTH_TWITTER_API_CONSUMER_KEY');
                    $consumer_secret = Setting::getValue('AUTH_TWITTER_API_CONSUMER_SECRET');
                    $connection = new Abraham\TwitterOAuth\TwitterOAuth($consumer_key, $consumer_secret);
                    $request_token = $connection->oauth('oauth/request_token', ['oauth_callback' => $domain.'/twlogin']);
                    $oauth_token = $request_token['oauth_token'];
                    $oauth_token_secret = $request_token['oauth_token_secret'];
                    
                    $loginTwitterUrl = $connection->url('oauth/authorize', ['oauth_token' => $oauth_token]);
                    echo '<a href="'.$loginTwitterUrl.'" class="btn btn-block btn-social btn-twitter btn-flat"><i class="fa fa-twitter"></i> Sign in using Twitter</a>';
                }       
            ?>
        </div>
        <a href="<?=Url::toRoute('/login')?>" class="text-center">I already have a membership</a><br>
      </div><!-- /.form-box -->
    </div>