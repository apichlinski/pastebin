<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;    
use yii\bootstrap\ActiveForm;

$this->title = 'Trends';
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile(Url::home() . 'plugins/datatables/dataTables.bootstrap.css');
        
$this->registerJsFile(Url::home() . 'plugins/datatables/jquery.dataTables.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJsFile(Url::home() . 'plugins/datatables/dataTables.bootstrap.min.js', ['depends' => 'yii\bootstrap\BootstrapPluginAsset']);
$this->registerJs("
    $(function () {
      $('#example2').DataTable({
        'order': [[ 3, 'desc' ]],
        'paging': true,
        'lengthChange': false,
        'searching': false,
        'ordering': true,
        'info': true,
        'autoWidth': false
      });
    });");


?>
<div class="box box-success">
    <div class="box-header">
      <h3 class="box-title">Most viewed Pastes</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
      <table id="example2" class="table table-bordered table-hover">
        <thead>
          <tr>
            <th>Title</th>
            <th>Type</th>
            <th>Added</th>
            <th>Views</th>
            <th>User</th>
          </tr>
        </thead>
        <tbody>
          <?php
            foreach ($pastesList as $model) {
              $pasteTitle = Html::encode($model->title);
              $pasteUrl = Url::toRoute('/paste/' . $model->getId() );
              $pasteAdded = $model->insert_date;
              $pasteViews = $model->views;
              $pasteType = $model->highlighting->name;
              $user = $model->author;
              if ($user!==false) {
                  $userUrl = Url::toRoute('/user/' . $user->user_name );
                  $userName = $user->user_name;

              }
              ?>
              <tr>
                  <td class="col-md-5"><a href="<?= $pasteUrl ?>" title="<?= $pasteTitle ?>"><?= $pasteTitle ?></a></td>
                  <td class="col-md-1"><?= $pasteType ?></td>
                  <td class="col-md-2"><?= $pasteAdded ?></td>
                  <td class="col-md-1"><?= $pasteViews ?></td>
                  <td class="col-md-2">
                      <?php if ($user!==false) { ?>
                          <a href="<?= $userUrl ?>" title="<?= $userName ?>"><?=$userName?></a>
                      <?php } else { ?>
                          Anonymous
                      <?php } ?>
                  </td>
              </tr>
              <?php
          } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>Title</th>
                <th>Type</th>
                <th>Added</th>
                <th>Views</th>
                <th>User</th>
            </tr>
        </tfoot>
      </table>
    </div><!-- /.box-body -->
  </div><!-- /.box -->