<div class="box box-primary">
    <div class="box-header with-border">
      <h3 class="box-title">About Me</h3>
    </div><!-- /.box-header -->
    <div class="box-body">
    <strong><i class="fa fa-transgender margin-r-5"></i> Sex</strong>
      <p class="text-muted"><?= $model->getDisplaySex() ?></p>

      <hr>

      <strong><i class="fa fa-book margin-r-5"></i>  Education</strong>
      <p class="text-muted">
        <?= $model->user_education?$model->user_education:'N/A' ?>
      </p>

      <hr>

      <strong><i class="fa fa-map-marker margin-r-5"></i> Location</strong>
      <p class="text-muted"><?= $model->user_location?$model->user_location:'N/A' ?></p>

      <hr>

      <strong><i class="fa fa-pencil margin-r-5"></i> Skills</strong>
      <p>
        <!--
        <span class="label label-danger">UI Design</span>
        <span class="label label-success">Coding</span>
        <span class="label label-info">Javascript</span>
        <span class="label label-warning">PHP</span>
        <span class="label label-primary">Node.js</span>
        -->
        <?= $model->user_skills?$model->user_skills:'N/A' ?>
      </p>

      <hr>

      <strong><i class="fa fa-file-text-o margin-r-5"></i> About</strong>
      <p><?= $model->user_description?$model->user_description:'N/A'?></p>
    </div><!-- /.box-body -->
  </div>