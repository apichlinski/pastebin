<?php 
use yii\helpers\Html;
use app\components\L;
use yii\widgets\Pjax; 
?>

<div class="<?=$tab=='comments'?'active':'' ?>  tab-pane" id="comments">
    <?php Pjax::begin(['id'=>'comments_list']); ?>
    <div id="all_comments">              
    <?php if ($comments) { 
        foreach ($comments as $comment) { ?>
            <!-- Post -->
            <div class="post" id="<?='comment_'.$comment->getId();?>">
                <div class="user-block">
                    <?php
                        if ($comment->author) {  
                        ?>
                            <img class="img-circle img-bordered-sm" src="<?= $comment->author->getUserPhotoUrl() ?>" alt="user image">
                                <span class="username">
                                  <a href="<?= $comment->author->getUserProfileUrl() ?>"><?= $comment->author->getDisplayName() ?></a>
                                  <!--<a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>-->
                              </span>
                        <?php
                        } else {
                            ?>
                            <span class="username">
                                Guests
                            </span>
                            <?php
                        }
                    ?>                              
                    <span class="description">Commented publicly - <?= L::timeAgo($comment->comment_creation_date); ?></span>
                </div><!-- /.user-block -->
                <p><?= $comment->comment_content ?></p>
                <ul class="list-inline">
                    <li>
                        <a href="<?=$comment->getPasteUrl()?>#<?='comment_'.$comment->getId();?>" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a>
                    </li>
                    <li>
                    <?= Html::a('<i class="fa fa-thumbs-o-up margin-r-5"></i> Like', '#', [
                        'data'=>[
                            'method' => 'post',
                            'params'=>['comment-like-button'=>'1', 'comment_id' => $comment->comment_id ],
                        ],
                        'class'=>'link-black text-sm likeclicked',
                    ]) ?>
                    <?= Html::a('<i class="fa fa-thumbs-o-down margin-r-5"></i> Unlike', '#', [
                        'data'=>[
                            'method' => 'post',
                            'params'=>['comment-unlike-button'=>'1', 'comment_id' => $comment->comment_id ],
                        ],
                        'class'=>'link-black text-sm likeclicked',
                    ]) ?>
                        (<?= $comment->countLikes() ?>)
                    </li>
                </ul>  
            </div><!-- /.post -->
        <?php } } else { ?>
            <p>No Comments found.</p>
        <?php } ?>
    </div>
    <?php Pjax::end(); ?>
    </div>