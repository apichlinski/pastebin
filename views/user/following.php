<?php

use app\components\L;

?>
<div class="<?=$tab=='following'?'active':'' ?> tab-pane" id="following">
	<!-- The timeline -->
	  <?php if ($followings) { ?>
	       <ul class="timeline timeline-inverse">
	        <?php foreach ($followings as $following) {
	        ?>                
	            <!-- timeline item -->
	            <li>
	              <i class="fa fa-user bg-aqua"></i>
	              <div class="timeline-item">
	                <span class="time"><i class="fa fa-clock-o"></i> <?= L::timeAgo($following->follow_date); ?></span>
	                <h3 class="timeline-header no-border">
	                    <?php if ($following->followed!=null){?>
	                        <a href="<?= $following->followed->getUserProfileUrl() ?>"><?= $following->followed->getDisplayName()?></a>
	                    <?php }else { echo 'Deleted'; }?>
	                    accepted your friend request
	                </h3>
	              </div>
	            </li>
	            
	            <!-- END timeline item -->
	<?php } ?>
	    <li>
	        <i class="fa fa-clock-o bg-gray"></i>
	    </li>
	</ul>
	<?php
	} else { ?>
	    <p>No following found.</p>
	  <?php }?>
	  

	<!-- timeline time label -->
	</div>