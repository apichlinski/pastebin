<?php

use yii\helpers\Html;
use yii\helpers\Url;
use app\components\HtmlHelper;
use app\components\L;
use yii\bootstrap\ActiveForm;
use kartik\file\FileInput;
use app\widgets\ImagePicker;
use app\models\Setting;


$user = Yii::$app->user->identity;

$this->title = 'Profil '.ucfirst($model->user_name);

?>
<div class="row">
    <div class="col-md-3">

      <!-- Profile Image -->
      <?php 
        if ($model->group!=false) {
            $userGroup = $model->group->user_group_name;
        } else {
            $userGroup = 'User';
        }
      ?>
      <div class="box box-<?= $model->isAdmin()?'danger':'primary'?>">
        <div class="box-body box-profile">
          <img class="profile-user-img img-responsive img-circle" src="<?=$model->getUserPhotoUrl()?>" alt="User profile picture">
          <h3 class="profile-username text-center"><?=ucfirst($model->getDisplayName())?></h3>
          <p class="text-muted text-center"><?=$model->isAdmin()?$userGroup:'Registered user'?></p>

          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b>Followers</b> <a class="pull-right"><?= $model->countUserFollowing() ?></a>
            </li>
            <li class="list-group-item">
              <b>Following</b> <a class="pull-right"><?= $model->countUserFollowed() ?></a>
            </li>
            <li class="list-group-item">
              <b>Pastes</b> <a class="pull-right"  href="<?=Url::toRoute('/search/' . $model->user_name ) ?>" title="Show user pastes"><?= $model->countUserPastes() ?></a>
            </li>
          </ul>
          
          <?php 
            if ($user!=false) { 
                if ($user->user_id != $model->user_id) {
                    if ($user->isFollowingUser($model->user_id) ) { 
                        $form = ActiveForm::begin(); ?>
                            <button type="submit" class="btn btn-primary btn-block" name="unfollow-button">Unfollow</button>
                        <?php ActiveForm::end(); 
                    }
                    else {
                        $form = ActiveForm::begin(); ?>
                            <button type="submit" class="btn btn-primary btn-block" name="follow-button">Follow</button>
                        <?php ActiveForm::end();
                    }
                }
            }?>
          
        </div><!-- /.box-body -->
      </div><!-- /.box -->

      <!-- About Me Box -->
      <?= $this->render(
            'about_me_box.php',
            ['model' => $model]
        ) ?>
      <!-- /.box -->
    </div><!-- /.col -->
        
    <div class="col-md-9">
        
        <?php if (Yii::$app->session->hasFlash('profile_saved')) { ?>
            <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <h4>	<i class="icon fa fa-check"></i> Success!</h4>
                <?= Yii::$app->session->getFlash('profile_saved') ?>
            </div>
        <?php } ?>
        
      <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
          <li <?=$tab=='active'?'class="active"':''?>><a href="#pastes" data-toggle="tab">Pastes</a></li>
          <li <?=$tab=='comments'?'class="active"':''?>><a href="#comments" data-toggle="tab">Comments</a></li>
          <li <?=$tab=='followers'?'class="active"':''?>><a href="#followers" data-toggle="tab">Followers</a></li>
          <li <?=$tab=='following'?'class="active"':''?>><a href="#following" data-toggle="tab">Following</a></li>
          <?php if ($user!=false) { ?>
          <?php if ($user->user_id == $model->user_id) { ?> 
            <?php /*<li <?=$tab=='avatar'?'class="active"':''?>><a href="#avatar" data-toggle="tab">Avatar</a></li> */ ?>
            <li <?=$tab=='settings'?'class="active"':''?>><a href="#settings" data-toggle="tab">Settings</a></li>
          <?php }  }?>
        </ul>
        <div class="tab-content">
          <?= $this->render(
              'pastes.php',
              ['tab' => $tab,
              'pastes' => $pastes,]
          ) ?>
          <!-- comments -->
          <?= $this->render(
              'comments.php',
              ['tab' => $tab,
              'comments' => $comments,]
          ) ?>          
          <!-- /.tab-pane -->
          <!-- followers -->
          <?= $this->render(
              'followers.php',
              ['tab' => $tab,
              'followers' => $followers,]
          ) ?>
          <!-- /.tab-pane -->
          <!-- following -->
          <?= $this->render(
              'following.php',
              ['tab' => $tab,
              'followings' => $following,]
          ) ?>
          <!-- /.tab-pane -->
          <!-- settings -->
          <?= $this->render(
              'settings.php',
              ['tab' => $tab,
              'formModel' => $formModel,]
          ) ?>
          <!-- /.tab-pane -->
        </div><!-- /.tab-content -->
      </div><!-- /.nav-tabs-custom -->
    </div><!-- /.col -->
  </div>
</div>
<?php  

$script = <<< JS
    
   $(document).ready(function () {
        $('body').on('beforeSubmit', 'form#comment-form', function () {
            alert('asdas');
            var form = $(this);
            // return false if form still have some validation errors
            if (form.find('.has-error').length) 
            {
                return false;
            }
            // submit form
            $.ajax({
            url    : form.attr('action'),
            type   : form.attr('method'),
            data   : form.serialize(),
            success: function (response) 
            {
                //var getupdatedata = $(responise).find('#all_comments');
                $.pjax.reload({container:'#comments_list'}); //for pjax update
                //$('#all_comments').html(getupdatedata);
                //console.log(getupdatedata);
            },
            error  : function () 
            {
                console.log('internal server error');
            }           
            });
            return false;
         });
        
        $('body').on('click', '.likeclicked', function () {
            var form = $(this);
            // submit form
            $.ajax({
            url    : '#',
            type   : form.data('method'),
            data   : form.data('params'),
            success: function (response) 
            {
                $.pjax.reload({container:'#comments_list'}); //for pjax update
            },
            error  : function () 
            {
                console.log('internal server error');
            }           
            });
            return false;
         });
    });
    
JS;
       
$this->registerJs($script);
?>