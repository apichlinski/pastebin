<?php
use app\components\L;
?>
<div class="<?=$tab=='active'?'active':'' ?> tab-pane" id="pastes">
  <!-- The timeline -->
  <?php if (count($pastes)>0) { ?>
  <ul class="timeline timeline-inverse">
      <?php foreach ($pastes as $paste) { ?>
          <!-- timeline item -->
          <li>
            <i class="fa fa-envelope bg-blue"></i>
            <div class="timeline-item">
              <span class="time"><i class="fa fa-clock-o"></i> <?= L::timeAgo($paste->insert_date); ?></span>
              <h3 class="timeline-header"><a href="#"><?= ($paste->author)?$paste->author->getDisplayName():'Guest' ?></a> sent an paste</h3>
              <div class="timeline-body">
                <?= $paste->content;?>
                <?php
                print_r($paste);
                 ?>
              </div>
              <div class="timeline-footer">
                <a class="btn btn-primary btn-xs" href="<?= $paste->getPasteUrl() ?>">Read more</a>
                <!--<a class="btn btn-danger btn-xs">Delete</a> -->
              </div>
            </div>
          </li>
          <!-- END timeline item -->
      <?php } ?>
    <li>
      <i class="fa fa-clock-o bg-gray"></i>
    </li>
  </ul>
  <?php } else { ?>
          <p>No pastes found.</p>
  <?php }  ?>
</div><!-- /.tab-pane -->