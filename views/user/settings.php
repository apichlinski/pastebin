<?php

use yii\bootstrap\ActiveForm;

?>
<div class="<?=$tab=='settings'?'active':'' ?> tab-pane" id="settings">
    <?php $form = ActiveForm::begin([
        'id' => 'form-horizontal',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'options' => [
                //'tag' => true,
                'class' => 'form-group has-feedback',
            ],
        ],
    ]); ?>
        <?= $form->field($formModel, 'username',[
            'template' => "{label}\n<div class='col-sm-10'>{input}\n{hint}\n{error}</div>",
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ])->textInput([
            'readonly' => true,
            'class'=>'form-control',
            'type'=>'text',                    
            'placeholder'=>'Name',    
            ]) ?>
    
        <?= $form->field($formModel, 'email',[
            'template' => "{label}\n<div class='col-sm-10'>{input}\n{hint}\n{error}</div>",
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ])->textInput([
            'readonly' => true,
            'class'=>'form-control',
            'type'=>'text',                    
            'placeholder'=>'E-mail address',    
            ]) ?>
    
        <?= $form->field($formModel, 'firstname',[
            'template' => "{label}\n<div class='col-sm-10'>{input}\n{hint}\n{error}</div>",
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ])->textInput([
            'class'=>'form-control',
            'type'=>'text',                    
            'placeholder'=>'Firstname',    
            ]) ?>
        
        <?= $form->field($formModel, 'lastname',[
            'template' => "{label}\n<div class='col-sm-10'>{input}\n{hint}\n{error}</div>",
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ])->textInput([
            'class'=>'form-control',
            'type'=>'text',                    
            'placeholder'=>'Lastname',    
            ]) ?>
    
        <?= $form->field($formModel, 'sex',[
            'template' => "{label}\n<div class='col-sm-10'>{input}\n{hint}\n{error}</div>",
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ])->dropDownList(
            ['K' => 'famale', 'M' => 'male'], ['prompt' => ''],
            [
                'class'=>'form-control',
                'type'=>'text',                    
                'placeholder'=>'Sex',    
            ]) ?>

        <?= $form->field($formModel, 'education',[
            'template' => "{label}\n<div class='col-sm-10'>{input}\n{hint}\n{error}</div>",
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ])->textInput([
            'class'=>'form-control',
            'type'=>'text',                    
            'placeholder'=>'Education',    
            ]) ?>

        <?= $form->field($formModel, 'location',[
            'template' => "{label}\n<div class='col-sm-10'>{input}\n{hint}\n{error}</div>",
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ])->textInput([
            'class'=>'form-control',
            'type'=>'text',                    
            'placeholder'=>'Location',    
            ]) ?>

        <?= $form->field($formModel, 'skills',[
            'template' => "{label}\n<div class='col-sm-10'>{input}\n{hint}\n{error}</div>",
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ])->textInput([
            'class'=>'form-control',
            'type'=>'text',                    
            'placeholder'=>'Skills',    
            ]) ?>
    
        <?= $form->field($formModel, 'about',[
            'template' => "{label}\n<div class='col-sm-10'>{input}\n{hint}\n{error}</div>",
            'labelOptions' => ['class' => 'col-sm-2 control-label'],
        ])->textArea([
            'style' => 'height: 200px',
            'class'=>'form-control',
            'type'=>'text',                    
            'placeholder'=>'About', 
            ]) ?>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-danger" name="save-button">Save</button>
            </div>
        </div>
    <?php ActiveForm::end(); ?>
  </div>