window.onload = function()
{
	$('.image-picker [data-role=remove]').click(function()
	{
		var ip = $(this).parents('.image-picker'), inp = ip.find('input')
		
		if (ip.data('url'))
		{
			$.ajax({
				url: ip.data('url'),
				type: 'POST',
				data: { name: inp.attr('name'), value: null },
				success: function(data)
				{
					ip.find('input').val('')
					ip.find('.thumbnail').css('background-image', data)
					ip.find('[data-role=remove]').addClass('disabled')
				}
			})
		}
		else
		{
			ip.find('input').val('')
			ip.find('.thumbnail').css('background-image', 'none')
			ip.find('[data-role=remove]').addClass('disabled')
		}
	})
	
	$('.image-picker [data-role=choose]').click(function()
	{
		$.ajax({
			url: '/profil/zdjecia',
			type: 'POST',
			data: { imagePicker: $(this).parents('.image-picker').attr('id') },
			success: function(data)
			{
				$('#image-picker .modal-body').html(data)
				$('#image-picker').modal('show')
			}
		})
	})
	
	$('#image-picker [data-role=choose]').click(function(e)
	{
		var img = $('#image-picker .image.active')
		
		if (img.length)
		{
			var ip = $('#' + $('#image-picker .gallery').data('id') + '.image-picker'), inp = ip.find('input'), id = img.data('id')
			
			if (ip.data('url'))
			{
				$.ajax({
					url: ip.data('url'),
					type: 'POST',
					data: { name: inp.attr('name'), value: id },
					success: function(data)
					{
						ip.find('input').val(id)
						ip.find('.thumbnail').css('background-image', data)
						ip.find('[data-role=remove]').removeClass('disabled')
					}
				})
			}
			else
			{
				ip.find('input').val(id)
				ip.find('.thumbnail').css('background-image', 'url(' + img.find('img').attr('src') + ')')
				ip.find('[data-role=remove]').removeClass('disabled')
			}
			
			$('#image-picker').modal('hide')
		}
		else
		{
			e.stopProppagation()
		}
	})
	
	$('#image-picker').click(function(e)
	{
		var t = $(e.target)
		
		if (!t.hasClass('image'))
		{
			t = t.parents('#image-picker .image')
		}
		
		if (t.hasClass('image'))
		{
			if (!t.hasClass('active'))
			{
				$('#image-picker .image').removeClass('active')
				t.addClass('active')
			}
		}
	})
}