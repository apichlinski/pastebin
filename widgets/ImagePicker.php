<?php

namespace app\widgets;

use Yii;
use yii\widgets\InputWidget;
use app\components\HtmlHelper;
use yii\helpers\Url;
use yii\helpers\Html;

class ImagePicker extends InputWidget
{
	public $url;
	
	public function run()
	{
		static $rendered = false;
		
		if (!$rendered)
		{
			$rendered = true;
			
			$this->getView()->registerJsFile(Url::home() . 'js/image-picker.js', ['position' => yii\web\View::POS_END]);
			
			echo '<div class="modal fade" id="image-picker">';
			echo '<div class="modal-dialog modal-lg">';
			echo '<div class="modal-content" style="height: 80%">';
			echo '<div class="modal-header">';
			echo '<button type="button" class="close" data-dismiss="modal" aria-label="Zamknij"><span aria-hidden="true">&times;</span></button>';
			echo '<h4 class="modal-title">Wybierz zdjęcie</h4>';
			echo '</div>';
			echo '<div class="modal-body" style="overflow-x: hidden; overflow-y: auto; max-height: 500px">';
			echo '</div>';
			echo '<div class="modal-footer">';
			echo '<button type="button" class="btn btn-default" data-dismiss="modal">Zamknij</button>';
			echo '<button data-role="choose" type="button" class="btn btn-primary">Wybierz</button>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
			echo '</div>';
		}
		
		if ($this->hasModel())
		{
			echo '<div class="image-picker"' . ($this->url ? ' data-url="' . $this->url . '"' : '') . ' id="' . $this->id . '">';
			echo Html::activeHiddenInput($this->model, $this->attribute, $this->options);
			echo '<div class="thumbnail" style="height: 200px';

		}
		else
		{
			echo '<div class="image-picker"' . ($this->url ? ' data-url="' . $this->url . '"' : '') . ' id="' . $this->id . '">';
			echo '<input type="hidden" name="' . $this->name . '" value="' . $this->value . '">';
			echo '<div class="thumbnail" style="height: 200px';
		}
		
		$imageID = $this->hasModel() ? $this->model->attributes[$this->attribute] : $this->value;
		
		if ($imageID)
		{
			if ($image = Yii::$app->db->createCommand('SELECT * FROM plik WHERE plik_id = :id')->bindValue(':id', $imageID)->queryOne())
			{
				echo '; background-image: url(' . HtmlHelper::getImagePath($image, 200, 200) . ')';
			}
		}
		
		echo '"></div>';
		echo '<div class="text-center">';
		echo '<div data-role="choose" class="btn btn-primary text-center" onclick="$(\'#imagePicker\').modal(\'show\')"><i class="fa fa-photo"></i> Wybierz zdjęcie</div> ';
		echo '<div data-role="remove" class="btn btn-warning text-center' . ($imageID ? '' : ' disabled') . '"><i class="fa fa-remove"></i> Usuń</div>';
		echo '</div>';
		echo '</div>';
	}
}